#pragma once

#include "Relation.hpp"

class RelationMatrix : public Relation, public std::enable_shared_from_this<RelationMatrix> {
public:
    typedef std::shared_ptr<RelationMatrix> PRelationMatrix;

    RelationMatrix() = default;

    explicit RelationMatrix(Matrix, std::string = " ");
	
    RelationMatrix(size_t, Type = Types::Empty);

    ~RelationMatrix() = default;

    RelationMatrix(const RelationMatrix &) = default;

    RelationMatrix(RelationMatrix &&) = default;

    RelationMatrix &operator=(const RelationMatrix &) = default;

    RelationMatrix &operator=(RelationMatrix &&) = default;

    PRelation getPtr() override;

    Matrix &getMutableMatrix() override;

    Matrix getMatrix() const override;

	TBase &at(size_t i, size_t j);

    void print(std::string) const override;
	
    PRelation intersect(PRelation) const override;

    PRelation unify(PRelation) const override;

    PRelation subtract(PRelation) const override;

    PRelation subtractSymmetrical(PRelation) const override;

    PRelation complement() const override;

    PRelation inverse() const override;

    PRelation compose(PRelation) const override;

    PRelation narrow(size_t) const override;

    bool contain(PRelation) const override;

    bool isReflexive() const override;

    bool isAntiReflexive() const override;

    bool isSymmethrical() const override;

    bool isASymmethrical() const override;

    bool isAntiSymmethrical() const override;

    bool isTransitive() override;

    bool isAcyclic() override;

    bool isConnected() override;

    bool isTolerant() const override;

    bool isEquivalent() override;

    bool isQuasiorder() override;

    bool isOrder() override;

    bool isStrictOrder() override;

    bool isLinearOrder() override;

    bool isStrictLinearOrder() override;

    bool equal(PRelation) const override;

    PRelation symmetricComponent() override;

    PRelation asymmetricComponent() override;

    PRelation transitiveLock() override;

    PRelation reach() override;

    PRelation mutualReach() override;

    PRelation factorization(FactorizationType) override;

	bool isMajorant(size_t value) const override;

    bool isMinorant(size_t value) const override;

    bool isMaximum(size_t value) const override;

    bool isMinimum(size_t value) const override;
	
protected:
    void empty() override;

    void full() override;

    void diagonal() override;

    void antidiagonal() override;

    PRelationMatrix power(PRelationMatrix, size_t);

    PRelationMatrix factorize(PRelationMatrix);

    Matrix matrix_;

private:
	template<size_t value>
    bool rowConsistsOnly(size_t) const;

	template<size_t value>
    bool colConsistsOnly(size_t) const;
};

template<size_t value>
inline bool RelationMatrix::rowConsistsOnly(size_t row) const {
    for (int i = 0; i < size(); ++i) {
        if (matrix_[row][i] != value) {
            return false;
        }
    }
    return true;
}

template<size_t value>
inline bool RelationMatrix::colConsistsOnly(size_t col) const {
    for (int i = 0; i < size(); ++i) {
        if (matrix_[i][col] != value) {
            return false;
        }
    }
    return true;
}

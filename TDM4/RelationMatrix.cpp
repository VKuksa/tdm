#include "RelationMatrix.hpp"

#include <iostream>
#include <iterator>

RelationMatrix::RelationMatrix(Matrix m, std::string name)
    : Relation(std::move(name)) {
    size_ = m.size();
    matrix_ = std::move(m);

    for (auto &col : matrix_)
        if (col.size() != size_)
            throw std::runtime_error("Invalid matrix size");
}

RelationMatrix::RelationMatrix(size_t size, Type type)
    : matrix_(size, std::vector<TBase>(size)) {
    size_ = size;

    switch (type) {
    case Types::Empty: {
        RelationMatrix::empty();
        break;
    }
    case Types::Full: {
        RelationMatrix::full();
        break;
    }
    case Types::Diagonal: {
        RelationMatrix::diagonal();
        break;
    }
    case Types::Antidiagonal: {
        RelationMatrix::antidiagonal();
        break;
    }
    default: {
        RelationMatrix::empty();
        break;
    }
    }
}

RelationMatrix::PRelation RelationMatrix::getPtr() {
    return shared_from_this();
}

RelationMatrix::Matrix &RelationMatrix::getMutableMatrix() {
    return matrix_;
}

RelationMatrix::Matrix RelationMatrix::getMatrix() const {
    return matrix_;
}

RelationMatrix::TBase &RelationMatrix::at(size_t i, size_t j) {
    return matrix_[i][j];
}

void RelationMatrix::print(std::string info) const {
    if (matrix_.empty())
        return;

    std::cout << "Relation: " << name_.data() << info.data() << std::endl;
    for (auto &el : matrix_) {
        std::copy(el.begin(), el.end(), std::ostream_iterator<TBase>(std::cout, "  "));
        std::cout << std::endl;
    }
}

void RelationMatrix::empty() {
    for (auto &row : matrix_)
        std::fill(row.begin(), row.end(), static_cast<TBase>(0));
}

void RelationMatrix::full() {
    for (auto &row : matrix_)
        std::fill(row.begin(), row.end(), static_cast<TBase>(1));
}

void RelationMatrix::diagonal() {
    this->empty();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (i == j)
                matrix_[i][j] = 1;
        }
    }
}

void RelationMatrix::antidiagonal() {
    this->empty();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (i == j)
                matrix_[i][j] = 0;
        }
    }
}

RelationMatrix::PRelation RelationMatrix::intersect(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<TBase>(size_, 0));
    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] == lmatrix_[i][j])
                result[i][j] = lmatrix_[i][j];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::unify(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<TBase>(size_, 0));
    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if ((this->matrix_[i][j] == 1) || (lmatrix_[i][j] == 1))
                result[i][j] = 1;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::subtract(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<TBase>(size_, 0));
    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] != lmatrix_[i][j])
                result[i][j] = this->matrix_[i][j];
            else
                result[i][j] = 0;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::subtractSymmetrical(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    auto first = this->subtract(left);
    auto second = left->subtract(std::const_pointer_cast<RelationMatrix>(shared_from_this()));

    return first->unify(second);
}

RelationMatrix::PRelation RelationMatrix::complement() const {
    Matrix result(size_, std::vector<TBase>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = 1 - matrix_[i][j];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::inverse() const {
    Matrix result(size_, std::vector<TBase>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = matrix_[j][i];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::compose(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    auto lmatrix_ = left->getMatrix();
    Matrix result(size_, std::vector<TBase>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = 0;
            for (size_t k = 0; k < size_; ++k) {
                result[i][j] += this->matrix_[i][k] * lmatrix_[k][j];
            }
            if (result[i][j] > 1)
                result[i][j] = 1;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::narrow(size_t excluded) const {
    if (excluded > size_) {
        std::cerr << "Invalid exclude parameter provided" << std::endl;
        return PRelation();
    }
    if (size_ == 1) {
        std::cerr << "Matrix cannot be narrowed" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<TBase>(size_ - 1, 0));

    for (size_t i = 0; i < size_; ++i) {
        if (i == excluded - 1)
            continue;
        for (size_t j = 0; j < size_ - 1; ++j) {
            result[i][j] = matrix_[i][j >= excluded - 1 ? j + 1 : j];
        }
    }
    result.erase(result.begin() + excluded - 1);

    return std::make_shared<RelationMatrix>(result);
}

bool RelationMatrix::contain(PRelation left) const {
    Matrix lmatrix_(left->getMatrix());

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (lmatrix_[i][j] == 1 && lmatrix_[i][j] != matrix_[i][j])
                return false;
        }
    }

    return true;
}

bool RelationMatrix::isReflexive() const {
    for (size_t i = 0; i < size(); ++i) {
        if (matrix_[i][i] != 1)
            return false;
    }
    return true;
}

bool RelationMatrix::isAntiReflexive() const {
    for (size_t i = 0; i < size(); ++i) {
        if (matrix_[i][i] != 0)
            return false;
    }
    return true;
}

bool RelationMatrix::isSymmethrical() const {
    for (size_t i = 0; i < size(); ++i) {
        for (size_t j = i + 1; j < size(); ++j) {
            if (matrix_[i][j] != matrix_[j][i])
                return false;
        }
    }
    return true;
}

bool RelationMatrix::isASymmethrical() const {
    auto empty = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<TBase>(size_, 0)));
    empty->empty();

    auto self = shared_from_this();
    auto inverse = self->inverse();
    return self->intersect(inverse)->equal(empty);
}

bool RelationMatrix::isAntiSymmethrical() const {
    for (int i = 0; i != size(); i++) {
        for (int j = 0; j != size(); j++) {
            if ((i != j) && matrix_[i][j] != 0 && matrix_[j][i] != 0) {
                return false;
            }
        }
    }
    return (true && !isASymmethrical());
}

bool RelationMatrix::isTransitive() {
    auto compose = shared_from_this()->compose(shared_from_this());
    return compose->contain(shared_from_this());
}

bool RelationMatrix::isAcyclic() {
    auto empty = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<TBase>(size_, 0)));
    empty->empty();
    return shared_from_this()->compose(shared_from_this())->intersect(shared_from_this()->inverse())->equal(empty);
}

bool RelationMatrix::isConnected() {
    auto self = shared_from_this();
    auto inversedSelf = self->inverse();

    auto diagonal = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<TBase>(size_, 0)));
    diagonal->diagonal();

    auto full = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<TBase>(size_, 0)));
    full->full();

    return (self->unify(inversedSelf)->subtract(diagonal)->equal(full->subtract(diagonal)));
}

bool RelationMatrix::isTolerant() const {
    return (this->isReflexive() && this->isSymmethrical());
}

bool RelationMatrix::isEquivalent() {
    return (this->isTolerant() && this->isTransitive());
}

bool RelationMatrix::isQuasiorder() {
    return (this->isReflexive() && this->isTransitive());
}

bool RelationMatrix::isOrder() {
    return (this->isQuasiorder() && this->isAntiSymmethrical());
}

bool RelationMatrix::isStrictOrder() {
    return (this->isASymmethrical() && this->isTransitive());
}

bool RelationMatrix::isLinearOrder() {
    return (this->isOrder() && this->isConnected());
}

bool RelationMatrix::isStrictLinearOrder() {
    return (this->isStrictOrder() && this->isConnected());
}

bool RelationMatrix::equal(PRelation left) const {
    if (left->size() != this->size())
        return false;

    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size(); ++i) {
        for (size_t j = 0; j < size(); ++j) {
            if (matrix_[i][j] != lmatrix_[i][j])
                return false;
        }
    }
    return true;
}

Relation::PRelation RelationMatrix::symmetricComponent() {
    auto self = shared_from_this();
    auto inversedSelf = self->inverse();

    return self->intersect(inversedSelf);
}

Relation::PRelation RelationMatrix::asymmetricComponent() {
    auto self = shared_from_this();
    auto selfSymmetciComponent = self->symmetricComponent();

    return self->subtract(selfSymmetciComponent);
}

Relation::PRelation RelationMatrix::transitiveLock() {
    auto self = shared_from_this();

    for (size_t i = 2; i < this->size(); ++i) {
        self = std::dynamic_pointer_cast<RelationMatrix>(self->unify(power(self, i)));
    }

    return self;
}

Relation::PRelation RelationMatrix::reach() {
    auto diagonal = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<TBase>(size_, 0)));
    diagonal->diagonal();

    return this->transitiveLock()->unify(diagonal);
}

Relation::PRelation RelationMatrix::mutualReach() {
    auto reach = this->reach();
    auto inversed = reach->inverse();
    return reach->intersect(inversed);
}

Relation::PRelation RelationMatrix::factorization(FactorizationType type) {
    std::shared_ptr<Relation> d;
    switch (type) {
    case FactorizationTypes::MutualReach:
        d = this->mutualReach();
        std::cout << "Using mutual reach: \n";
        break;
    case FactorizationTypes::SymmetricComponent:
        d = this->symmetricComponent();
        std::cout << "Using symmetric component: \n";
        break;
    default:
        d = shared_from_this();
        break;
    }

    return factorize(std::dynamic_pointer_cast<RelationMatrix>(d));
}

bool RelationMatrix::isMajorant(size_t col) const {
    return colConsistsOnly<0>(col);
}

bool RelationMatrix::isMinorant(size_t row) const {
    return rowConsistsOnly<0>(row);
}

bool RelationMatrix::isMaximum(size_t row) const {
    return rowConsistsOnly<1>(row);
}

bool RelationMatrix::isMinimum(size_t col) const {
    return colConsistsOnly<1>(col);
}

RelationMatrix::PRelationMatrix RelationMatrix::power(PRelationMatrix selfBasic, size_t current) {
    if (current == 1) {
        return selfBasic;
    } else {
        return std::dynamic_pointer_cast<RelationMatrix>(selfBasic->compose(power(selfBasic, current - 1)));
    }
}

RelationMatrix::PRelationMatrix RelationMatrix::factorize(PRelationMatrix d) {
    if (!d)
        return PRelationMatrix();

    auto p = shared_from_this();
    auto dmatrix_ = d->getMatrix();

    std::list<std::set<size_t>> factors;
    std::set<size_t> added;

    for (size_t i = 0; i < d->size() - 1; ++i) {
        if (std::find(added.begin(), added.end(), i) != added.end())
            continue;

        std::set<size_t> current{i};
        for (size_t j = i + 1; j < d->size(); ++j) {
            if (std::equal(dmatrix_[i].begin(), dmatrix_[i].end(), dmatrix_[j].begin())) {
                current.emplace(j);
                added.emplace(j);
            }
        }
        factors.emplace_back(current);
        added.emplace(i);
    }

    auto result = p->inverse()->getMatrix();
    for (const auto &set : factors) {
        if (set.size() > 1) {
            for (auto it = ++set.begin(); it != set.end(); ++++it) {
                auto firstCol = result.at(*--it);
                auto secondtCol = result.at(*++it);
                std::vector<TBase> resultCol(size(), 0);
                for (int i = 0; i < size(); ++i) {
                    if ((firstCol[i] == 1) || (secondtCol[i] == 1))
                        resultCol[i] = 1;
                }
                result[*--it] = std::move(resultCol);
            }
        }
    }

    auto cresult = Matrix(factors.size());
    auto rit = result.begin();
    for (auto it = cresult.begin(); it != cresult.end(); ++it, ++rit) {
        *it = std::vector<TBase>(rit->begin(), rit->begin() + factors.size());
    }

    return std::dynamic_pointer_cast<RelationMatrix>(std::make_shared<RelationMatrix>(cresult)->inverse());
}

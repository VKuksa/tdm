﻿#include "ChoiceMechanism.hpp"

#include <iostream>

namespace {
    const Alternatives c = {{8, 5, 7}, {3, 4, 6}, {2, 5, 3}, {2, 3, 6}};
    const Alternative etalon = {4, 4, 5};
	const LinearFurl importances = {0.5, 0.4, 0.1};
    const std::vector<size_t> order = {0, 1, 2};
    const std::vector<size_t> restriction = {4, 5};


    template<class... Fs>
    void unpack(Fs &&... fs) {
        (void(std::forward<Fs>(fs)()), ...);
    }

    template<class... Ts>
    void printChoices() {
        unpack([&] {
			std::shared_ptr<ChoiceMechanism> p = std::make_shared<Ts>(c);
            std::cout << p;
            p->performChoice();
            std::cout << "\n---------------------------------\n\n";
        }...);
    }
}

int main() {
    try {
        printChoices<Paretto, Sleiter, BestResult, AssuredResult, Gurvitz>();

        {
			std::shared_ptr<ChoiceMechanism> p = std::make_shared<Etalon>(c, etalon);
	        std::cout << p;
	        p->performChoice();
	        std::cout << "\n---------------------------------\n\n";
		}
        {
			std::shared_ptr<ChoiceMechanism> p = std::make_shared<CriteriaFurl>(c, importances);
	        std::cout << p;
	        p->performChoice();
	        std::cout << "\n---------------------------------\n\n";
		}
		{
			std::shared_ptr<ChoiceMechanism> p = std::make_shared<Lexicographical>(c, order);
	        std::cout << p;
	        p->performChoice();
	        std::cout << "\n---------------------------------\n\n";
		}
		{
			std::shared_ptr<ChoiceMechanism> p = std::make_shared<MainCriterion>(c, restriction);
	        std::cout << p;
	        p->performChoice();
	        std::cout << "\n---------------------------------\n\n";
		}

        std::cin.ignore();
        std::cin.get();
    } catch (const std::runtime_error &exc) {
        std::cerr << "Error occured: " << exc.what() << std::endl;
    }

    return 0;
}

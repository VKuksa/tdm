#include "ChoiceMechanism.hpp"

#include <algorithm>
#include <iostream>
#include <numeric>
#include <utility>

namespace {
    constexpr double alpha = 0.96;
}

ChoiceMechanism::ChoiceMechanism(std::string name, Alternatives alternatives)
    : name_(name) {
    relation_ = std::make_unique<RelationMatrix>(alternatives.size(), Relation::Type::Empty);

    std::copy(alternatives.begin(), alternatives.end(), std::back_inserter(alternatives_));
}

std::ostream &operator<<(std::ostream &os, ChoiceMechanism *a) {
    os << a->name() << "\n";
    os << "alternatives:\n";

    auto i = 1;
    for (const auto &alternative : a->alternatives_) {
        os << "Q(a" << i++ << ") = ( ";
        std::copy(alternative.begin(), alternative.end(), std::ostream_iterator<size_t>(std::cout, ", "));
        os << "\b\b )\n";
    }
    a->relation_->print();
    os << a->rule_.data() << std::endl;

    return os;
}

Paretto::Paretto(Alternatives alternatives)
    : ChoiceMechanism("Paretto", std::move(alternatives)) {
    rule_ = R"(R = <T\E, A+(T\E)>)";
    bool flag{true};

    for (int i = 0; i < alternatives_.size(); ++i) {
        for (int j = 0; j < alternatives_.size(); ++j) {
            flag = true;

            for (int k = 0; k < alternativeSize; ++k) {
                if (alternatives_[i][k] < alternatives_[j][k]) {
                    flag = false;
                    break;
                }
            }

            if (flag)
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void Paretto::performChoice() const {
    std::shared_ptr<Relation> result = std::make_shared<RelationMatrix>(relation_->size(), Relation::Types::Diagonal);
    result = relation_->subtract(result);

    printChoice([&](int i) { return result->isMajorant(i); });
}

Sleiter::Sleiter(Alternatives alternatives)
    : ChoiceMechanism("Sleiter", std::move(alternatives)) {
    rule_ = "R = <TvE, A+(TvE)>";
    bool flag{true};

    for (int i = 0; i < alternatives_.size(); ++i) {
        for (int j = 0; j < alternatives_.size(); ++j) {
            flag = true;

            for (int k = 0; k < alternativeSize; ++k) {
                if (alternatives_[i][k] <= alternatives_[j][k]) {
                    flag = false;
                    break;
                }
            }

            if (flag)
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void Sleiter::performChoice() const {
    std::shared_ptr<Relation> result = std::make_shared<RelationMatrix>(relation_->size(), Relation::Types::Diagonal);
    result = relation_->unify(result);

    printChoice([&](int i) { return result->isMaximum(i); });
}

BestResult::BestResult(Alternatives alternatives)
    : ChoiceMechanism("BestResult", std::move(alternatives)) {
    rule_ = "R = <T, A+(T)>";
    bool flag{true};

    for (int i = 0; i < alternatives_.size(); ++i) {
        for (int j = 0; j < alternatives_.size(); ++j) {
            flag = true;

            auto first = std::max_element(alternatives_[i].begin(), alternatives_[i].end());
            auto second = std::max_element(alternatives_[j].begin(), alternatives_[j].end());
            if (*first < *second) {
                flag = false;
                break;
            }

            if (flag)
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void BestResult::performChoice() const {
    printChoice([&](int i) { return relation_->isMaximum(i); });
}

AssuredResult::AssuredResult(Alternatives alternatives)
    : ChoiceMechanism("AssuredResult", std::move(alternatives)) {
    rule_ = "R = <T, A+(T)>";
    bool flag{true};

    for (int i = 0; i < alternatives_.size(); ++i) {
        for (int j = 0; j < alternatives_.size(); ++j) {
            flag = true;

            auto first = std::min_element(alternatives_[i].begin(), alternatives_[i].end());
            auto second = std::min_element(alternatives_[j].begin(), alternatives_[j].end());
            if (*first < *second) {
                flag = false;
                break;
            }

            if (flag)
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void AssuredResult::performChoice() const {
    printChoice([&](int i) { return relation_->isMaximum(i); });
}

Gurvitz::Gurvitz(Alternatives alternatives)
    : ChoiceMechanism("Gurvitz", std::move(alternatives)) {
    rule_ = "R = <T, A+(T)>";
    bool flag{true};
    auto pred = [&](size_t min, size_t max) -> double { return alpha * min + (1.0 - alpha) * max; };

    for (int i = 0; i < alternatives_.size(); ++i) {
        for (int j = 0; j < alternatives_.size(); ++j) {
            flag = true;

            auto maxF = std::max_element(alternatives_[i].begin(), alternatives_[i].end());
            auto minF = std::min_element(alternatives_[i].begin(), alternatives_[i].end());
            auto maxS = std::max_element(alternatives_[j].begin(), alternatives_[j].end());
            auto minS = std::min_element(alternatives_[j].begin(), alternatives_[j].end());
            if (pred(*maxF, *minF) <= pred(*maxS, *minS)) {
                flag = false;
                break;
            }

            if (flag)
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void Gurvitz::performChoice() const {
    printChoice([&](int i) { return relation_->isMaximum(i); });
}

Etalon::Etalon(Alternatives alternatives, Alternative etalon)
    : ChoiceMechanism("Etallon", std::move(alternatives)) {
    rule_ = "R = <T, A+(T)>";

    std::vector<int> distances(alternatives_.size(), 0);

    for (int i = 0; i < alternatives_.size(); i++) {
        for (int j = 0; j < alternativeSize; j++) {
            distances[i] += abs((int)alternatives_[i][j] - (int)etalon[j]);
        }
    }

    for (int i = 0; i < alternatives_.size(); i++) {
        for (int j = 0; j < alternatives_.size(); j++) {
            if (distances[i] <= distances[j])
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void Etalon::performChoice() const {
    printChoice([&](int i) { return relation_->isMaximum(i); });
}

CriteriaFurl::CriteriaFurl(Alternatives alternatives, LinearFurl importances)
    : ChoiceMechanism("CriteriaFurl", std::move(alternatives)) {
    if (std::accumulate(importances.begin(), importances.end(), 0.0) != 1.0) {
        throw std::runtime_error("Importances summ not equal 1");
    }
    rule_ = "R = <T, A+(T)>";

    for (auto &alternative : alternatives) {
        for (int i = 0; i < alternative.size(); ++i) {
            alternative[i] = static_cast<int>(importances[i] * alternative[i]);
        }
    }

    bool flag{true};

    for (int i = 0; i < alternatives_.size(); ++i) {
        for (int j = 0; j < alternatives_.size(); ++j) {
            flag = true;

            for (int k = 0; k < alternativeSize; ++k) {
                if (alternatives_[i][k] <= alternatives_[j][k]) {
                    flag = false;
                    break;
                }
            }

            if (flag)
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }
}

void CriteriaFurl::performChoice() const {
    std::shared_ptr<Relation> result = std::make_shared<RelationMatrix>(relation_->size(), Relation::Types::Diagonal);
    result = relation_->unify(result);

    printChoice([&](int i) { return result->isMaximum(i); });
}

Lexicographical::Lexicographical(Alternatives alternatives, const std::vector<size_t> order)
    : ChoiceMechanism("Lexicographical", std::move(alternatives)) {
    rule_ = "R = <T, A+(T)>";

    for (size_t i = 0; i < alternatives_.size(); ++i) {
        for (size_t j = 0; j < alternatives_.size(); ++j) {
            for (size_t k = 0; k < alternativeSize; ++k) {
                if (alternatives_[i][order[k]] > alternatives_[j][order[k]]) {
                    relation_->getMutableMatrix()[i][j] = 1;
                    break;
                } else if (alternatives_[i][order[k]] < alternatives_[j][order[k]]) {
                    relation_->getMutableMatrix()[j][i] = 1;
                    break;
                }
            }
        }
    }
}

void Lexicographical::performChoice() const {
    std::shared_ptr<Relation> result = std::make_shared<RelationMatrix>(relation_->size(), Relation::Types::Diagonal);
    result = relation_->subtract(result);

    printChoice([&](int i) { return result->isMajorant(i); });
}

MainCriterion::MainCriterion(Alternatives alternatives, const std::vector<size_t> restriction)
    : ChoiceMechanism("MainCriterion", std::move(alternatives)) {
    rule_ = "R = <T, A+(T)>";

    for (size_t i = 0; i < alternatives_.size(); ++i) {
        for (size_t j = 0; j < alternatives_.size(); ++j) {
            if (alternatives_[i][0] >= alternatives_[j][0])
                relation_->getMutableMatrix()[i][j] = 1;
        }
    }

    for (int i = 0; i < alternatives_.size(); i++) {
        if (alternatives_[i][1] < restriction[0] || alternatives_[i][2] < restriction[1]) { //for 3 criterias GAVNOKOD
            for (int k = 0; k < alternatives_.size(); k++) {
                relation_->getMutableMatrix()[i][k] = relation_->getMutableMatrix()[k][i] = 0;
            }
            forDeletion_.push_back(i);
        }
    }
}

void MainCriterion::performChoice() const {
    std::shared_ptr<Relation> result = std::make_shared<RelationMatrix>(relation_->getMutableMatrix());

    std::list<size_t> alternatives;
    int deleted = -1;
    for (const auto &elem : forDeletion_) {
        result = result->narrow(elem - deleted);
        deleted++;
    }

    for (int i = 0; i < result->size(); ++i) {
        if (result->isMaximum(i)) {
            auto pos = std::find(forDeletion_.begin(), forDeletion_.end(), i);
            if (pos != forDeletion_.end()) {
                if (*++pos == ++i)
                    continue;
            }
            alternatives.push_back(i + 1);
        }
    }

    if (!alternatives.empty()) {
        std::cout << "Result of choice are : {";
        std::copy(alternatives.begin(), alternatives.end(), std::ostream_iterator<size_t>(std::cout, ", "));
        std::cout << "\b\b}" << std::endl;
    } else {
        std::cout << "Result of choice is empty union" << std::endl;
    }
}

#pragma once
#include "RelationMatrix.hpp"

#include <queue>
#include <functional>

class ChoiceMechanism {
public:
    ChoiceMechanism(std::string name, Alternatives alternatives);

    virtual ~ChoiceMechanism() = default;

    virtual void performChoice() const = 0;

    decltype(auto) name() const {
        return name_.data();
    };

    friend std::ostream &operator<<(std::ostream &, ChoiceMechanism *);

protected:
    template<typename T>
    void printChoice(T predicate) const;

    Alternatives alternatives_;
    std::unique_ptr<Relation> relation_;
    std::string rule_;
    std::string name_;
};

class Paretto final : public ChoiceMechanism {
public:
    explicit Paretto(Alternatives alternatives);

    void performChoice() const override;
};

class Sleiter final : public ChoiceMechanism {
public:
    explicit Sleiter(Alternatives alternatives);

    void performChoice() const override;
};

class BestResult final : public ChoiceMechanism {
public:
    explicit BestResult(Alternatives alternatives);

    void performChoice() const override;
};

class AssuredResult final : public ChoiceMechanism {
public:
    explicit AssuredResult(Alternatives alternatives);

    void performChoice() const override;
};

class Gurvitz final : public ChoiceMechanism {
public:
    explicit Gurvitz(Alternatives alternatives);

    void performChoice() const override;
};

class Etalon final : public ChoiceMechanism {
public:
    explicit Etalon(Alternatives alternatives, Alternative etallon);

    void performChoice() const override;
};

class CriteriaFurl final : public ChoiceMechanism {
public:
    explicit CriteriaFurl(Alternatives alternatives, LinearFurl importances);

    void performChoice() const override;
};

class Lexicographical final : public ChoiceMechanism {
public:
    explicit Lexicographical(Alternatives alternatives, const std::vector<size_t> order);

    void performChoice() const override;
};

class MainCriterion final : public ChoiceMechanism {
public:
    explicit MainCriterion(Alternatives alternatives, const std::vector<size_t> restriction);

    void performChoice() const override;

private:
    std::list<size_t> forDeletion_;
};

template<typename T>
inline void ChoiceMechanism::printChoice(T predicate) const {
    std::list<size_t> alternatives;
    for (int i = 0; i < relation_->size(); ++i) {
        if (predicate(i))
            alternatives.push_back(i + 1);
    }

    if (!alternatives.empty()) {
        std::cout << "Result of choice are : {";
        std::copy(alternatives.begin(), alternatives.end(), std::ostream_iterator<size_t>(std::cout, ", "));
        std::cout << "\b\b}" << std::endl;
    } else {
        std::cout << "Result of choice is empty union" << std::endl;
    }
}

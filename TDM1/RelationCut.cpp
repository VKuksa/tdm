#include "RelationCut.hpp"

#include "RelationMatrix.hpp"

#include <iterator>
#include <numeric>
#include <set>

namespace {
    template<typename T>
    std::set<T> extractDuplicates(const std::set<T> &first, const std::set<T> &second) {
        std::set<T> duplicates;
        for (const auto &elem : second)
            if (std::find(first.begin(), first.end(), elem) != first.end())
                duplicates.insert(elem);

        return duplicates;
    }

    template<typename T>
    std::set<T> extractUnified(const std::set<T> &first, const std::set<T> &second) {
        std::set<T> unified(first.begin(), first.end());
        for (const auto &elem : second)
            unified.insert(elem);

        return unified;
    }

    template<typename T>
    std::set<T> extractSubstract(const std::set<T> &first, const std::set<T> &second) {
        std::set<T> unique(first.begin(), first.end());
        for (const auto &elem : second) {
            auto pos = std::find(first.begin(), first.end(), elem);
            if (pos != first.end())
                unique.erase(elem);
        }

        return unique;
    }
}

RelationCut::RelationCut(Cut c)
    : Relation(c.size())
    , cut_(std::move(c)) {}

RelationCut::RelationCut(IntMatrix matrix) {
    size_ = matrix.size();
    Cut cut;

    for (int i = 0; i < size_; ++i) {
        std::set<int> col;
        for (int j = 0; j < size_; ++j) {
            if (matrix[j][i] == 1) {
                col.emplace(j + 1);
            }
        }
        cut[i + 1] = std::move(col);
    }
    cut_ = std::move(cut);
}

RelationCut::RelationCut(size_t size)
    : Relation(size) {}

RelationCut::PRelation RelationCut::getPtr() {
    return shared_from_this();
}

Cut RelationCut::getCut() const {
    return cut_;
}

IntMatrix RelationCut::getMatrix() const {
    IntMatrix matrix(size_, std::vector<int>(size_, 0));

    for (const auto &row : cut_) {
        if (!row.second.empty()) {
            for (const auto &elem : row.second)
                matrix[elem - 1][row.first - 1] = 1;
        }
    }

    return matrix;
}

void RelationCut::print(std::string name) const {
    if (cut_.empty())
        return;

    std::cout << "Relation: " << name.data() << std::endl;
    for (const auto &el : cut_) {
        std::cout << "R+(" << el.first << ") = { ";

        if (!el.second.empty())
            std::copy(el.second.begin(), el.second.end(), std::ostream_iterator<int>(std::cout, ", "));
        else
            std::cout << " ";

        std::cout << "\b\b }" << std::endl;
    }
}

void RelationCut::empty() {
    for (size_t i = size_; i != 0; --i) {
        cut_[i] = std::set<int>{};
    }
}

void RelationCut::full() {
    for (size_t i = size_; i != 0; --i) {
        std::vector<int> vec(size_);
        std::iota(vec.begin(), vec.end(), 1);
        cut_[i] = std::set<int>(vec.begin(), vec.end());
    }
}

void RelationCut::diagonal() {
    for (int i = static_cast<int>(size_); i != 0; --i) {
        cut_[i] = std::set<int>{i};
    }
}

void RelationCut::antidiagonal() {
    for (int i = static_cast<int>(size_); i != 0; --i) {
        cut_[i] = std::set<int>{static_cast<int>(size_) + 1 - i};
    }
}

RelationCut::PRelation RelationCut::intersect(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid cut size provided" << std::endl;
        return PRelation();
    }

    Cut result;
    auto lcut_ = left->getCut();

    try {
        for (const auto &el : cut_) {
            result[el.first] = extractDuplicates(el.second, lcut_.at(el.first));
        }
    } catch (const std::out_of_range &exc) {
        std::cerr << "Maps aren't equal: " << exc.what() << std::endl;
        return PRelation();
    }

    return std::make_shared<RelationCut>(result);
}

RelationCut::PRelation RelationCut::unify(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid cut size provided" << std::endl;
        return PRelation();
    }

    Cut result;
    auto lcut_ = left->getCut();

    try {
        for (const auto &el : cut_) {
            result[el.first] = extractUnified(el.second, lcut_.at(el.first));
        }
    } catch (const std::out_of_range &exc) {
        std::cerr << "Maps aren't equal: " << exc.what() << std::endl;
        return PRelation();
    }

    return std::make_shared<RelationCut>(result);
}

RelationCut::PRelation RelationCut::subtract(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid cut size provided" << std::endl;
        return PRelation();
    }

    Cut result;
    auto lcut_ = left->getCut();

    try {
        for (const auto &el : cut_) {
            result[el.first] = extractSubstract(el.second, lcut_.at(el.first));
        }
    } catch (const std::out_of_range &exc) {
        std::cerr << "Maps aren't equal: " << exc.what() << std::endl;
        return PRelation();
    }

    return std::make_shared<RelationCut>(result);
}

RelationCut::PRelation RelationCut::subtractSymmetrical(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid cut size provided" << std::endl;
        return PRelation();
    }

    auto first = this->subtract(left);
    auto second = left->subtract(shared_from_this());

    return first->unify(second);
}

RelationCut::PRelation RelationCut::complement() {
    Cut result;

    try {
        for (int i = 1; i <= size_; ++i) {
            std::vector<int> vec(size_);
            std::iota(vec.begin(), vec.end(), 1);
            result[i] = extractSubstract(std::set<int>(vec.begin(), vec.end()), cut_.at(i));
        }
    } catch (const std::out_of_range &exc) {
        std::cerr << "Maps isn't valid: " << exc.what() << std::endl;
        return PRelation();
    }

    return std::make_shared<RelationCut>(result);
}

RelationCut::PRelation RelationCut::inverse() {
    return std::make_shared<RelationCut>(std::make_shared<RelationMatrix>(this->getMatrix())->inverse()->getCut());
}

RelationCut::PRelation RelationCut::compose(PRelation left) {
    return std::make_shared<RelationCut>(std::make_shared<RelationMatrix>(this->getMatrix())->compose(left)->getCut());
}

RelationCut::PRelation RelationCut::narrow(size_t excluded) {
    return std::make_shared<RelationCut>(std::make_shared<RelationMatrix>(this->getMatrix())->narrow(excluded)->getCut());
}

bool RelationCut::contain(PRelation left) {
    Cut lcut_(left->getCut());

    try {
        for (int i = 1; i <= size_; ++i) {
            for (auto &elem : lcut_.at(i))
                if (std::find(cut_.at(i).begin(), cut_.at(i).end(), elem) == cut_.at(i).end())
                    return false;
        }
    } catch (const std::out_of_range &exc) {
        std::cerr << "Maps isn't valid: " << exc.what() << std::endl;
        return false;
    }

    return true;
}

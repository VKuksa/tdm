﻿#pragma once

#include "Relation.hpp"

class RelationCut : public Relation, public std::enable_shared_from_this<Relation> {
public:
    typedef std::shared_ptr<RelationCut> PRelationCut;

    RelationCut() = default;

    explicit RelationCut(Cut);

    explicit RelationCut(IntMatrix);

    explicit RelationCut(size_t);

    ~RelationCut() = default;

    RelationCut(const RelationCut &) = default;

    RelationCut(RelationCut &&) = default;

    RelationCut &operator=(const RelationCut &) = default;

    RelationCut &operator=(RelationCut &&) = default;

    PRelation getPtr() override;

    Cut getCut() const override;

    IntMatrix getMatrix() const override;

    void print(std::string name) const override;

    void empty() override;

    void full() override;

    void diagonal() override;

    void antidiagonal() override;

    PRelation intersect(PRelation) override;

    PRelation unify(PRelation) override;

    PRelation subtract(PRelation) override;

    PRelation subtractSymmetrical(PRelation) override;

    PRelation complement() override;

    PRelation inverse() override;

    PRelation compose(PRelation) override;

    PRelation narrow(size_t) override;

    bool contain(PRelation) override;

private:
    Cut cut_;
};

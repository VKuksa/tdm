﻿#pragma once

#include "..\types.h"

#include <iostream>
#include <vector>

class Relation {
public:
    typedef std::shared_ptr<Relation> PRelation;

    Relation() = default;

    Relation(size_t size)
        : size_{size} {}

    virtual PRelation getPtr() = 0;

    virtual Cut getCut() const = 0;

    virtual IntMatrix getMatrix() const = 0;

    virtual void print(std::string name) const = 0;

    virtual void empty() = 0;

    virtual void full() = 0;

    virtual void diagonal() = 0;

    virtual void antidiagonal() = 0;

    virtual PRelation intersect(PRelation) = 0; //p

    virtual PRelation unify(PRelation) = 0; //o

    virtual PRelation subtract(PRelation) = 0; //r

    virtual PRelation subtractSymmetrical(PRelation) = 0; //sr

    virtual PRelation complement() = 0;

    virtual PRelation inverse() = 0;

    virtual PRelation compose(PRelation) = 0;

    virtual PRelation narrow(size_t) = 0;

    virtual bool contain(PRelation) = 0;

    virtual ~Relation() = default;

    size_t size() const {
        return size_;
    }

protected:
    size_t size_{0};
};

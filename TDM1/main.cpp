﻿#include "Relation.hpp"
#include "RelationCut.hpp"
#include "RelationMatrix.hpp"
#include "..\types.h"

#include <chrono>

namespace {
    /*const IntMatrix P = {{0, 1, 1, 0, 1}, {0, 0, 1, 0, 0}, {0, 1, 0, 0, 1}, {0, 0, 1, 0, 1}, {0, 0, 0, 0, 0}};
    const IntMatrix Q = {{1, 1, 0, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 1, 1, 0}, {0, 0, 1, 1, 0}, {1, 1, 0, 0, 1}};
    const IntMatrix R = {{0, 0, 0, 1, 0}, {0, 0, 1, 1, 1}, {0, 1, 0, 0, 1}, {0, 0, 0, 1, 0}, {0, 0, 1, 1, 0}};*/

	const IntMatrix P = { {1, 0, 1, 1, 0}, {0, 1, 1, 0, 1}, {1, 1, 1, 0, 0}, {1, 0, 0, 1, 1}, {0, 1, 0, 1, 1} };
	const IntMatrix Q = { {1, 0, 1, 0, 0}, {0, 1, 0, 1, 1}, {1, 0, 1, 1, 1}, {0, 1, 0, 1, 1}, {0, 1, 0, 1, 1} };
	const IntMatrix R = { {1, 1, 1, 1, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 1, 0}, {0, 0, 0, 0, 0}, {0, 0, 1, 0, 0} };


    //const IntMatrix P = {{0, 0, 0, 0, 0}, {0, 0, 1, 0, 1}, {1, 0, 1, 0, 0}, {1, 0, 1, 0, 1}, {0, 1, 0, 0, 1}};
    //const IntMatrix Q = {{0, 0, 0, 0, 0}, {0, 0, 1, 1, 0}, {0, 1, 0, 0, 1}, {0, 0, 0, 0, 0}, {0, 0, 1, 0, 0}};
    //const IntMatrix R = {{1, 1, 0, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 1, 1, 0}, {0, 0, 1, 1, 0}, {1, 1, 0, 0, 1}};

    template<typename TimeT = std::chrono::microseconds>
    struct measure {
        template<typename F, typename... Args>
        static typename TimeT::rep execution(F &&func, Args &&... args) {
            auto start = std::chrono::steady_clock::now();
            std::invoke(std::forward<decltype(func)>(func), std::forward<Args>(args)...);
            auto duration = std::chrono::duration_cast<TimeT>(std::chrono::steady_clock::now() - start);
            return duration.count();
        }
    };

	auto all = [](std::shared_ptr<Relation> p, std::shared_ptr<Relation> q)
	{
		p->intersect(q)->print("intersect");
		p->unify(q)->print("unify");
		p->subtract(q)->print("substract");
		q->subtract(p)->print("rev substract");
		p->subtractSymmetrical(q)->print("symm substract");
		p->complement()->print("complement");
		p->inverse()->print("inverse");
		p->compose(q)->print("compose");
		p->narrow(2)->print("narrow");
	};
}


int main() {
    try {
		 {
	        std::shared_ptr<Relation> p = std::make_shared<RelationMatrix>(P);
	        std::shared_ptr<Relation> q = std::make_shared<RelationMatrix>(Q);
	        std::shared_ptr<Relation> r = std::make_shared<RelationMatrix>(R);

	        std::shared_ptr<Relation> k;

			std::cout << "IntMatrix method presentation relation type" << std::endl;

	        p->print("P");
	        q->print("Q");
	        r->print("R");

	        auto cfunctor = [&]() { k = p->compose(q)->subtract(r->complement()->inverse()); };
			
            std::cout << "IntMatrix method execution type: " << measure<>::execution(cfunctor) << std::endl;
	        if (k) {
	            k->print("K");
	        }
        }
       std::cout << "\n------------------------------\n\n";
        {
            std::shared_ptr<Relation> p = std::make_shared<RelationCut>(P);
            std::shared_ptr<Relation> q = std::make_shared<RelationCut>(Q);
            std::shared_ptr<Relation> r = std::make_shared<RelationCut>(R);

            std::shared_ptr<Relation> k;

            std::cout << "Cut method presentation relation type" << std::endl;

            p->print("P");
            q->print("Q");
            r->print("R");

            auto cfunctor = [&]() { k = p->compose(q)->subtract(r->complement()->inverse()); };
			
	        std::cout << "Cut method execution type: " << measure<>::execution(cfunctor) << std::endl;
            if (k) {
                k->print("K");
            }
        }
        std::cin.get();
    } catch (const std::runtime_error &exc) {
        std::cerr << "Error occured: " << exc.what() << std::endl;
    }

    return 0;
}

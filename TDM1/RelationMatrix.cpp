#include "RelationMatrix.hpp"

#include <iterator>

RelationMatrix::RelationMatrix(IntMatrix m)
    : Relation(m.size()) {
    for (auto &col : m)
        if (col.size() != size_)
            throw std::runtime_error("Invalid matrix size");
    matrix_ = std::move(m);
}

RelationMatrix::RelationMatrix(Cut cut) {
    size_ = cut.size();
    IntMatrix matrix(size_, std::vector<int>(size_, 0));

    for (const auto &row : cut) {
        if (!row.second.empty()) {
            for (const auto &elem : row.second)
                matrix[elem - 1][row.first - 1] = 1;
        }
    }

    matrix_ = std::move(matrix);
}

RelationMatrix::RelationMatrix(size_t size, int filler)
    : Relation(size)
    , matrix_(size_, std::vector<int>(size_, filler)) {}

RelationMatrix::PRelation RelationMatrix::getPtr() {
    return shared_from_this();
}

Cut RelationMatrix::getCut() const {
    Cut cut;
    for (int i = 0; i < size_; ++i) {
        std::set<int> col;
        for (int j = 0; j < size_; ++j) {
            if (matrix_[j][i] == 1) {
                col.emplace(j + 1);
            }
        }
        cut[i + 1] = std::move(col);
    }
    return cut;
}

IntMatrix RelationMatrix::getMatrix() const {
    return matrix_;
}

void RelationMatrix::print(std::string name) const {
    if (matrix_.empty())
        return;

    std::cout << "Relation: " << name.data() << std::endl;
    for (auto &el : matrix_) {
        std::copy(el.begin(), el.end(), std::ostream_iterator<int>(std::cout, "  "));
        std::cout << std::endl;
    }
}

void RelationMatrix::empty() {
    for (auto &row : matrix_)
        std::fill(row.begin(), row.end(), 0);
}

void RelationMatrix::full() {
    for (auto &row : matrix_)
        std::fill(row.begin(), row.end(), 1);
}

void RelationMatrix::diagonal() {
    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (i == j)
                matrix_[i][j] = 1;
        }
    }
}

void RelationMatrix::antidiagonal() {
    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (i == j)
                matrix_[i][j] = 0;
        }
    }
}

RelationMatrix::PRelation RelationMatrix::intersect(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    IntMatrix result(size_, std::vector<int>(size_, 0));
    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] == lmatrix_[i][j])
                result[i][j] = lmatrix_[i][j];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::unify(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    IntMatrix result(size_, std::vector<int>(size_, 0));
    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if ((this->matrix_[i][j] == 1) || (lmatrix_[i][j] == 1))
                result[i][j] = 1;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::subtract(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    IntMatrix result(size_, std::vector<int>(size_, 0));
    auto lmatrix_ = left->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] != lmatrix_[i][j])
                result[i][j] = this->matrix_[i][j];
            else
                result[i][j] = 0;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::subtractSymmetrical(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    auto first = this->subtract(left);
    auto second = left->subtract(shared_from_this());

    return first->unify(second);
}

RelationMatrix::PRelation RelationMatrix::complement() {
    IntMatrix result(size_, std::vector<int>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = 1 - matrix_[i][j];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::inverse() {
    IntMatrix result(size_, std::vector<int>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = matrix_[j][i];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::compose(PRelation left) {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    auto lmatrix_ = left->getMatrix();
    IntMatrix result(size_, std::vector<int>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = 0;
            for (size_t k = 0; k < size_; ++k) {
                result[i][j] += this->matrix_[i][k] * lmatrix_[k][j];
            }
            if (result[i][j] > 1)
                result[i][j] = 1;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

RelationMatrix::PRelation RelationMatrix::narrow(size_t excluded) {
    if (excluded > size_) {
        std::cerr << "Invalid exclude parameter provided" << std::endl;
        return PRelation();
    }
    if (size_ == 1) {
        std::cerr << "IntMatrix cannot be narrowed" << std::endl;
        return PRelation();
    }

    IntMatrix result(size_, std::vector<int>(size_ - 1, 0));

    for (size_t i = 0; i < size_; ++i) {
        if (i == excluded - 1)
            continue;
        for (size_t j = 0; j < size_ - 1; ++j) {
            result[i][j] = matrix_[i][j >= excluded - 1 ? j + 1 : j];
        }
    }
    result.erase(result.begin() + excluded - 1);

    return std::make_shared<RelationMatrix>(result);
}

bool RelationMatrix::contain(PRelation left) {
    IntMatrix lmatrix_(left->getMatrix());

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (lmatrix_[i][j] == 1 && lmatrix_[i][j] != matrix_[j][i])
                return false;
        }
    }

    return false;
}

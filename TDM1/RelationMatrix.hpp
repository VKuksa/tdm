#pragma once

#include "Relation.hpp"

class RelationMatrix : public Relation, public std::enable_shared_from_this<RelationMatrix> {
public:
    typedef std::shared_ptr<RelationMatrix> PRelationMatrix;

    RelationMatrix() = default;

    explicit RelationMatrix(IntMatrix);

    explicit RelationMatrix(Cut);

    explicit RelationMatrix(size_t, int);

    ~RelationMatrix() = default;

    RelationMatrix(const RelationMatrix &) = default;

    RelationMatrix(RelationMatrix &&) = default;

    RelationMatrix &operator=(const RelationMatrix &) = default;

    RelationMatrix &operator=(RelationMatrix &&) = default;

    PRelation getPtr() override;

    Cut getCut() const override;

    IntMatrix getMatrix() const override;

    void print(std::string name) const override;

    void empty() override;

    void full() override;

    void diagonal() override;

    void antidiagonal() override;

    PRelation intersect(PRelation) override;

    PRelation unify(PRelation) override;

    PRelation subtract(PRelation) override;

    PRelation subtractSymmetrical(PRelation) override;

    PRelation complement() override;

    PRelation inverse() override;

    PRelation compose(PRelation) override;

    PRelation narrow(size_t) override;

    bool contain(PRelation) override;

private:
    IntMatrix matrix_;
};

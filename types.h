#pragma once

#include <array>
#include <set>
#include <unordered_map>

constexpr size_t alternativeSize = 3;
constexpr size_t relationsQuantity = 5;

constexpr size_t methodsSize = 4;

template<typename T>
using TMatrix = std::vector<std::vector<T>>;

template<typename T>
using TCut = std::unordered_map<size_t, std::set<T>>;

using IntMatrix = TMatrix<int>;
using FloatMatrix = TMatrix<float>;

typedef std::unordered_map<size_t, std::set<int>> Cut;

typedef std::array<size_t, alternativeSize> Alternative;
typedef std::vector<Alternative> Alternatives;

typedef std::array<double, alternativeSize> LinearFurl;

typedef std::vector<size_t> SubclassesQuantity;
typedef std::vector<size_t> ElementQuantity;
typedef std::vector<std::set<int>> Classes;

typedef std::vector<std::pair<size_t, size_t>> Interface; 

template<typename T>
using TArray = std::vector<T>;

using TArrayD = TArray<double>;

template<typename T>
using Matrix = TArray<TArray<T>>;
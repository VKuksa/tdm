﻿#include "..\types.h"
#include "RelationMatrixMetr.hpp"

namespace {
	//13
   /* const FloatMatrix P = {{0, 0, 0, 3, 0}, {3, 0, 0, 6, 1}, {4, 1, 0, 7, 2}, {0, 0, 0, 0, 0}, {2, 0, 0, 5, 0}};
    const FloatMatrix Q = {{0, 0, 0, 0, 0}, {1, 0, 0, 0, 0}, {5, 4, 0, 2, 3}, {3, 2, 0, 0, 1}, {2, 1, 0, 0, 0}};
    const FloatMatrix P1 = {{1, 0, 0, 2, 10}, {0, 1, 0, 0, 0}, {0, 0, 1, 0, 6}, {0, 0, 0, 1, 5}, {0, 0, 0, 0, 1}};
    const FloatMatrix Q1 = {{1, 0, 0, 0, 0}, {0, 1, 2, 0, 8}, {0, 0, 1, 0, 4}, {1, 1, 2, 1, 8}, {0, 0, 0, 0, 1}};
    */
	//4
	const FloatMatrix P = {{0, 0, 0, 0, 0}, {7, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {5, 0, 0, 0, 0}, {9, 2, 0, 4, 0}};
    const FloatMatrix Q = {{0, 0, 0, 5, 0}, {2, 0, 0, 7, 1}, {4, 2, 0, 9, 3}, {0, 0, 0, 0, 0}, {1, 0, 0, 6, 0}};
    const FloatMatrix P1 = {{1, 0, 0, 0, 5}, {0, 1, 0, 0, 0}, {0, 0, 1, 0, 0}, {0, 10, 0, 1, 0}, {0, 0, 0, 0, 1}};
    const FloatMatrix Q1 = {{1, 3, 6, 6, 12}, {0, 1, 2, 2, 4}, {0, 0, 1, 1, 2}, {0, 0, 1, 1, 2}, {0, 0, 0, 0, 1}};

	auto allActions = [](std::shared_ptr<Relation> p,  std::shared_ptr<Relation> q) { 
		std::cout << std::boolalpha << std::endl;
       
        p->print();
        std::cout << "Additive: " << p->isAdditive();
        std::cout << "\nMultiplicative: " << p->isMultiplicative();
        std::cout << "\nConcerned: " << p->isConcerned() << "\n\n";

        p->print();
        std::cout << "Additive: " << p->isAdditive();
        std::cout << "\nMultiplicative: " << p->isMultiplicative();
        std::cout << "\nConcerned: " << p->isConcerned() << "\n\n";

		p->compose(q)->print("Composition");
        p->subtract(q)->print("Substraction");
        p->intersect(q)->print("Intersection");
        p->unify(q)->print("Unification");
	};
}

int main() {
    try {
        std::shared_ptr<Relation> p = std::make_shared<RelationMatrixMetr>(P, "P");
        std::shared_ptr<Relation> q = std::make_shared<RelationMatrixMetr>(Q, "Q");
        std::shared_ptr<Relation> p1 = std::make_shared<RelationMatrixMetr>(P1, "P1");
        std::shared_ptr<Relation> q1 = std::make_shared<RelationMatrixMetr>(Q1, "Q1");

		allActions(p, q);
        allActions(p1, q1);
        
        std::cin.get();
    } catch (const std::exception &exc) {
        std::cerr << "Error occured: " << exc.what() << std::endl;
    }

    return 0;
}

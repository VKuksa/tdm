﻿#include "RelationMatrixMetr.hpp"

#include <functional>
#include <iomanip>
#include <iterator>

namespace {
    namespace additive {
        const auto pred = [](const float left, const float right, float &sum) { return sum += (left + right); };

        const auto interunification = [](const float left, const float right) { return ((left + right) / 2); };

        const auto operation = [](const float sum, const int count) { return sum / count; };
    }
    namespace multiplicative {
        const auto pred = [](const float left, const float right, float &mul) { return mul *= (left * right); };

        const auto interunification = [](const float left, const float right) { return sqrt((left * right)); };

        const auto operation = [](const float mul, const int count) { return static_cast<float>(pow(mul, (1.0 / count))); };
    }
}

RelationMatrixMetr::RelationMatrixMetr(Matrix m, std::string name)
    : RelationMatrix(std::move(m), std::move(name)) {}

RelationMatrixMetr::RelationMatrixMetr(Cut cut)
    : RelationMatrix(std::move(cut)) {}

RelationMatrixMetr::RelationMatrixMetr(size_t size, Type type)
    : RelationMatrix(size, type) {}

Relation::PRelation RelationMatrixMetr::intersect(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }
    Matrix result(size_, std::vector<BaseT>(size_));
    auto lmatrix_ = left->getMatrix();
    std::function<float(const float, const float)> operation;

    if (this->isAdditive() && left->isAdditive()) {
        operation = additive::interunification;
    } else if (this->isMultiplicative() && left->isMultiplicative()) {
        operation = multiplicative::interunification;
    } else {
        std::cout << "Matrices are not additive or multiplicative. Intersection was not calculated." << std::endl;
        return PRelation();
    }

    for (size_t i = 0; i < size(); i++) {
        for (size_t j = 0; j < size(); j++) {
            if (matrix_[i][j] == 0. || lmatrix_[i][j] == 0.)
                result[i][j] = 0;
            else
                result[i][j] = operation(matrix_[i][j], lmatrix_[i][j]);
        }
    }

    return std::make_shared<RelationMatrixMetr>(result);
}

Relation::PRelation RelationMatrixMetr::unify(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<BaseT>(size_));
    auto lmatrix_ = left->getMatrix();
    std::function<float(const float, const float)> operation;

    if (this->isAdditive() && left->isAdditive()) {
        operation = additive::interunification;
    } else if (this->isMultiplicative() && left->isMultiplicative()) {
        operation = multiplicative::interunification;
    } else {
        std::cout << "Matrices are not additive or multiplicative. Association was not calculated." << std::endl;
        return PRelation();
    }

    for (int i = 0; i < size(); i++) {
        for (int j = 0; j < size(); j++) {
            if (matrix_[i][j] == 0. || lmatrix_[i][j] == 0.)
                result[i][j] = matrix_[i][j] + lmatrix_[i][j];
            else
                result[i][j] = operation(matrix_[i][j], lmatrix_[i][j]);
        }
    }

    return std::make_shared<RelationMatrixMetr>(result);
}

Relation::PRelation RelationMatrixMetr::subtract(PRelation left) const { //TODO review it
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<BaseT>(size_));
    auto lmatrix_ = left->getMatrix();

    for (int i = 0; i < size(); i++) {
        for (int j = 0; j < size(); j++) {
            if (matrix_[i][j] != 0. && !lmatrix_[i][j])
                result[i][j] = matrix_[i][j];
            else
                result[i][j] = 0;
        }
    }

    return std::make_shared<RelationMatrixMetr>(result);
}

Relation::PRelation RelationMatrixMetr::compose(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<BaseT>(size_));
    auto lmatrix_ = left->getMatrix();
    std::function<float(const float, const float, float &)> predicate;
    std::function<float(const float, const int)> operation;

    if (this->isAdditive() && left->isAdditive()) {
        predicate = additive::pred;
        operation = additive::operation;
    } else if (this->isMultiplicative() && left->isMultiplicative()) {
        predicate = multiplicative::pred;
        operation = additive::operation;
    } else {
        std::cout << "Matrices are not additive or multiplicative. Ordinary composition for calculated." << std::endl;
        return PRelation();
    }

    float value = 0.;
    int count = 0;
    for (int i = 0; i < size(); i++) {
        for (int j = 0; j < size(); j++) {
            for (int k = 0; k < size(); k++) {
                if (matrix_[i][k] != 0 && lmatrix_[k][j] != 0) {
                    predicate(matrix_[i][k], lmatrix_[k][j], value);
                    count++;
                }
            }
            if (count != 0)
                result[i][j] = operation(value, count);
            else
                result[i][j] = 0;
            count = 0;
            value = 0;
        }
    }

    return std::make_shared<RelationMatrixMetr>(result);
}

Relation::BaseT RelationMatrixMetr::getV(size_t i, size_t j) const {
    if (i < size() && j < size())
        return matrix_[i][j];
    else
        throw std::logic_error("Invalid index");
}

void RelationMatrixMetr::setV(size_t i, size_t j, BaseT value) {
    if (i < size() && j < size())
        matrix_[i][j] = value;
    else
        throw std::logic_error("Invalid index");
}

bool RelationMatrixMetr::isAdditive() const {
    auto isAdditive{true};

    for (int i = 0; i < size(); i++)
        for (int j = 0; j < size(); j++) {
            if (matrix_[i][j]) {
                for (int k = 0; k < size(); k++)
                    if (matrix_[j][k]) {
                        if (matrix_[i][j] + matrix_[j][k] == matrix_[i][k])
                            continue;
                        else {
                            isAdditive = false;
                            break;
                        }
                    }
            }
        }

    return isAdditive;
}

bool RelationMatrixMetr::isMultiplicative() const {
    auto isMultiplicative{true};

    for (int i = 0; i < size(); i++)
        for (int j = 0; j < size(); j++) {
            if (matrix_[i][j]) {
                for (int k = 0; k < size(); k++)
                    if (matrix_[j][k]) {
                        if (matrix_[i][j] * matrix_[j][k] == matrix_[i][k])
                            continue;
                        else {
                            isMultiplicative = false;
                            break;
                        }
                    }
            }
        }

    return isMultiplicative;
}

bool RelationMatrixMetr::isConcerned() const {
    auto isConcerned{true};

    for (size_t i = 0; i < size(); i++) {
        for (size_t j = i; j < size(); j++) {
            if (matrix_[i][j] != -matrix_[j][i]) {
                isConcerned = 0;
                i = size();
                break;
            }
        }
    }

    return isConcerned;
}

void RelationMatrixMetr::print(std::string info) const {
    if (matrix_.empty())
        return;

    std::cout << "Metricized relation: " << name_.data() << info.data() << std::endl;
    for (const auto &row : matrix_) {
        for (const auto &el : row) {
            std::cout << std::setw(5) << std::setprecision(3) << el;
        }
        std::cout << std::endl;
    }
}

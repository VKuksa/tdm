#include "RelationGroup.hpp"
//#define HELPERS

namespace {
	namespace helpers
	{
		auto printInterface = [](const Interface &inter) {
#ifdef HELPERS
			std::cout << std::endl;
			for(const auto &elem : inter)
			{
				std::cout << "Elem: " << elem.first << " : " << elem.second << std::endl;
			}
#endif
		};
	}

	auto sortL = [](Interface::value_type a, Interface::value_type b) {
		return a.second < b.second;
	}; 
}

RelationGroup::RelationGroup(Relations relations)
    : relations_(std::move(relations))
    , interfacesMatrix_(relations.begin()->size(), Relation::Type::Empty)
    , interfacesList_(relations.begin()->size(), {0, 0}) {
	for (size_t i = 0; i < size(); ++i)	{
		for (size_t k = 0; k < size(); ++k)	{
			for (size_t l = 0; l < size(); ++l)	{
				interfacesList_[k].first = k;
				interfacesList_[k].second += relations_[i].at(k, l);
			}
		}
		std::sort(interfacesList_.begin(), interfacesList_.end(), sortL);

		for (size_t j = 0; j < size(); j++)
		{
			interfacesMatrix_.at(j, i) = static_cast<int>(interfacesList_[j].first);
		}
	}
}

constexpr size_t RelationGroup::size() const noexcept {
	return relations_.size();
}

RelationMatrix<> RelationGroup::interfaces() const {
	return interfacesMatrix_;
}

void RelationGroup::printRelation() const {
	for (size_t i = 0; i < size(); i++)
	{
		std::cout << "E" << i + 1 << "\t";
	}
	std::cout << "\n";

	for (size_t i = 0; i < size(); i++)
	{
		for (size_t j = 0; j < size(); j++)
		{
			std::cout << "x" << interfaces().at(i, j) + 1 << "\t";
		}
		std::cout << std::endl;
	}
}

void RelationGroup::printName() const {
	std::string name = typeid(*this).name();
	std::cout << "Method:" << name.substr(name.find_first_of(' ')).c_str() << "\n";
}

VoteMajority::VoteMajority(Relations relations) : RelationGroup(std::move(relations)) {
	printName();
	helpers::printInterface(interfacesList_);

	for (size_t i = 0; i < size(); ++i)	{
		interfacesList_[interfacesMatrix_.at(0,i)].second++;
	}
	std::sort(interfacesList_.begin(), interfacesList_.end(), sortL);
	helpers::printInterface(interfacesList_);

	auto prev = interfacesList_[0].second;
	for(size_t i = 0; i < size();) {
		std::cout << "\nY" << i + 1 << " - ";

		for (; i < 5; i++) {
			if (interfacesList_[i].second == prev) {
				std::cout << "x" << interfacesList_[i].first + 1 << " ";
			} else {
				prev = interfacesList_[i].second;
				break;
			}
		}
	}
}

Condorse::Condorse(Relations relations) : RelationGroup(std::move(relations)) {
	printName();

	std::vector<std::vector<size_t>> S(5, std::vector<size_t>(5, 0));
	std::vector<std::vector<size_t>> B(5, std::vector<size_t>(5, 0));

	for (size_t i = 0; i < size(); ++i)	{
		for (size_t j = i + 1; j < size(); ++j)	{
			for (size_t k = 0; k < size(); ++k)	{

				size_t temp1 = 0, temp2 = 0;
				for (size_t l = 0; l < size(); ++l)	{
					if (interfacesMatrix_.at(l,k) == static_cast<int>(i)) {
						temp1 = l;
					}
				}

				for (size_t l = 0; l < size(); ++l)	{
					if (interfacesMatrix_.at(l,k) == static_cast<int>(j)) {
						temp2 = l;
					}
				}

				if (temp1 < temp2)
					S[i][j]++;
				else
					S[j][i]++;
			}
		}
	}

	for (size_t i = 0; i < size(); i++) {
		for (size_t j = 0; j < size(); j++)	{
			if (S[i][j] >= S[j][i] && i != j)
				B[i][j] = 1;
		}
	}

	for (size_t i = 0; i < size(); i++)	{
		for (size_t j = 0; j < size(); j++) {
			interfacesList_[i].second += B[i][j];
		}
	}
	
	helpers::printInterface(interfacesList_);

	for (size_t i = 0; i < size(); ++i)	{
		std::cout << "\nY" << i + 1 << " - ";
		std::cout << "x" << interfacesList_[i].first + 1;
	}
}

Bord::Bord(Relations relations) : RelationGroup(std::move(relations)) {
	printName();
	helpers::printInterface(interfacesList_);

	for (size_t i = 0; i < size(); ++i)	{
		for (size_t j = 0; j < size(); ++j) {
			interfacesList_[interfacesMatrix_.at(j,i)].second += 4 - j;
		}
	}
	std::sort(interfacesList_.begin(), interfacesList_.end(), sortL);
	helpers::printInterface(interfacesList_);

	for (size_t i = 0; i < size(); ++i)	{
		std::cout << "\nY" << i + 1 << " - ";
		std::cout << "x" << interfacesList_[i].first + 1;
	}
}

KemeniMedian::KemeniMedian(Relations relations) : RelationGroup(std::move(relations)) {
	printName();
	
	std::vector<std::vector<size_t>> loose(5, std::vector<size_t>(5, 0));

	for (size_t i = 0; i < size(); ++i) {
		interfacesList_[i].second = 0;
		interfacesList_[i].first = i;
	}

	for (size_t i = 0; i < size(); ++i)	{
		for (size_t j = 0; j < size(); ++j) {
			if (i != j)
				for (size_t k = 0; k < size(); ++k)	{
					loose[i][j] += 1 - (interfacesMatrix_.at(i,j) - interfacesMatrix_.at(j,i));
				}
		}
	}

	std::vector<size_t> res(5);
	std::vector<size_t> index = { 0,1,2,3,4 };
	auto sz = size();

	do {
		for (size_t i = 0; i < size(); ++i) {
			for (size_t j = 0; j < size(); ++j) {
				interfacesList_[i].second += loose[index[i]][index[j]];
			}
		}
		
		std::sort(interfacesList_.begin(), interfacesList_.end(), sortL);
		res[5 - sz] = interfacesList_[sz - 1].first;

		for (size_t i = 0; i < sz - 1; ++i) {
			index[i] = interfacesList_[i].first;
		}

		if (--sz == 0)
			break;

		std::sort(index.begin(), index.end(), std::greater<size_t>());
	} while (sz >= 0);

	for (size_t i = 0; i < size(); ++i)
	{
		std::cout << "\tx" << res[i] + 1;
	}
}

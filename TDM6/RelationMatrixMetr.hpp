#pragma once

#include "RelationMatrix.hpp"

class RelationMatrixMetr : public RelationMatrix<float>, public std::enable_shared_from_this<RelationMatrixMetr> {
public:
    typedef std::shared_ptr<RelationMatrixMetr> PRelationMatrixMetr;

    typedef TMatrix<float> Matrix;

    RelationMatrixMetr() = default;

    explicit RelationMatrixMetr(Matrix, std::string = "");

    explicit RelationMatrixMetr(size_t, Type);

    PRelation intersect(PRelation) const override;

    PRelation unify(PRelation) const override;

    PRelation subtract(PRelation) const override;

    PRelation compose(PRelation) const override;

    ~RelationMatrixMetr() = default;

    RelationMatrixMetr(const RelationMatrixMetr &) = default;

    RelationMatrixMetr(RelationMatrixMetr &&) = default;

    RelationMatrixMetr &operator=(const RelationMatrixMetr &) = default;

    RelationMatrixMetr &operator=(RelationMatrixMetr &&) = default;

    float getV(size_t i, size_t j) const;

    void setV(size_t i, size_t j, float value);

    bool isAdditive() const override;

    bool isMultiplicative() const override;

    bool isConcerned() const override;

    void print(std::string = "") const override;
};

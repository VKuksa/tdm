#pragma once
#include "../types.h"
#include "RelationMatrix.hpp"

typedef std::array<RelationMatrix<>, relationsQuantity> Relations;

class RelationGroup {
public:
    RelationGroup(Relations relations);

	virtual ~RelationGroup() = default;

	constexpr size_t size() const noexcept;

	RelationMatrix<> interfaces() const;

protected:
	void printName() const;

	void printRelation() const;

    Relations relations_;
	RelationMatrix<> interfacesMatrix_;
	Interface interfacesList_;
};

class VoteMajority final : public RelationGroup {
public:
    explicit VoteMajority(Relations relations);
};

class Condorse final : public RelationGroup {
public:
    explicit Condorse(Relations relations);
};

class Bord final : public RelationGroup {
public:
    explicit Bord(Relations relations);
};

class KemeniMedian final : public RelationGroup {
public:
	explicit KemeniMedian(Relations relations);
};


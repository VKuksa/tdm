﻿#include "RelationGroup.hpp"

namespace {
	const IntMatrix P1 = {{0, 0, 0, 0, 0}, {1, 0, 0, 0, 0}, {1, 1, 0, 0, 0}, {1, 1, 1, 0, 1}, {1, 1, 1, 0, 0}};
	const IntMatrix P2 = {{0, 0, 1, 0, 1}, {1, 0, 1, 1, 1}, {0, 0, 0, 0, 1}, {1, 0, 1, 0, 1}, {0, 0, 0, 0, 0}};
	const IntMatrix P3 = {{0, 0, 1, 1, 1}, {1, 0, 1, 1, 1}, {0, 0, 0, 0, 1}, {0, 0, 1, 0, 1}, {0, 0, 0, 0, 0}};
	const IntMatrix P4 = {{0, 0, 0, 0, 0}, {1, 0, 1, 0, 0}, {1, 0, 0, 0, 0}, {1, 1, 1, 0, 0}, {1, 1, 1, 1, 0}};
	const IntMatrix P5 = {{0, 0, 0, 0, 0}, {1, 0, 1, 1, 0}, {1, 0, 0, 0, 0}, {1, 0, 1, 0, 0}, {1, 1, 1, 1, 0}};

    RelationMatrix<> p1(P1, "P1");
    RelationMatrix<> p2(P2, "P2");
    RelationMatrix<> p3(P3, "P3");
    RelationMatrix<> p4(P4, "P4");
    RelationMatrix<> p5(P5, "P5");
	
    Relations relations{p1, p2, p3, p4, p5};

	template<class... Fs>
    void unpack(Fs &&... fs) {
        (void(std::forward<Fs>(fs)()), ...);
    }

    template<class... Ts>
    void printChoices() {
        unpack([&] {
			std::shared_ptr<RelationGroup> p = std::make_shared<Ts>(relations);
            std::cout << "\n---------------------------------\n\n";
        }...);
    }
}

int main() {
    try {
        printChoices<VoteMajority, Condorse, Bord, KemeniMedian>();

        std::cin.get();
    } catch (const std::exception &exc) {
        std::cerr << "Error occured: " << exc.what() << std::endl;
    }

    return 0;
}

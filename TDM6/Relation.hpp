﻿#pragma once

#include "..\types.h"

#include <iostream>

class Relation {
public:
    typedef enum class FactorizationTypes { MutualReach = 1, SymmetricComponent = 2 } FactorizationType;

    typedef enum class Types { Empty, Diagonal, Full, Antidiagonal } Type;

    typedef std::shared_ptr<Relation> PRelation;

    Relation() = default;

    explicit Relation(std::string name)
        : name_(std::move(name)) {}

    Relation(size_t size)
        : size_{size} {}

    virtual PRelation getPtr() = 0;

    virtual void print(std::string info = "") const = 0;

    virtual void empty() = 0;

    virtual void full() = 0;

    virtual void diagonal() = 0;

    virtual void antidiagonal() = 0;

    virtual PRelation intersect(PRelation) const = 0; //p

    virtual PRelation unify(PRelation) const = 0; //o

    virtual PRelation subtract(PRelation) const = 0; //r

    virtual PRelation subtractSymmetrical(PRelation) const = 0; //sr

    virtual PRelation complement() const = 0;

    virtual PRelation inverse() const = 0;

    virtual PRelation compose(PRelation) const = 0;

    virtual PRelation narrow(size_t) const = 0;

    virtual bool contain(PRelation) const = 0;

    virtual bool isReflexive() const = 0;

    virtual bool isAntiReflexive() const = 0;

    virtual bool isSymmethrical() const = 0;

    virtual bool isASymmethrical() const = 0;

    virtual bool isAntiSymmethrical() const = 0;

    virtual bool isTransitive() = 0;

    virtual bool isAcyclic() = 0;

    virtual bool isConnected() = 0;

    virtual bool equal(PRelation) const = 0;

    virtual bool isTolerant() const = 0;

    virtual bool isEquivalent() = 0;

    virtual bool isQuasiorder() = 0;

    virtual bool isOrder() = 0;

    virtual bool isStrictOrder() = 0;

    virtual bool isLinearOrder() = 0;

    virtual bool isStrictLinearOrder() = 0;

    virtual PRelation symmetricComponent() = 0;

    virtual PRelation asymmetricComponent() = 0;

    virtual PRelation transitiveLock() = 0;

    virtual PRelation reach() = 0;

    virtual PRelation mutualReach() = 0;

    virtual PRelation factorization(FactorizationType) = 0;

    virtual bool isAdditive() const {
        return false;
    }

    virtual bool isMultiplicative() const {
        return false;
    }

    virtual bool isConcerned() const {
        return false;
    }

    virtual ~Relation() = default;

    size_t size() const {
        return size_;
    }

protected:
    std::string name_{""};

    size_t size_{0};
};

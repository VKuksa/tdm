#include "Criterials.h"
#include <map>

namespace {
    constexpr auto DoubleMin = std::numeric_limits<double>::min();
    constexpr auto DoubleMax = std::numeric_limits<double>::max();
}

int Bayyesa_Laplassa(const Matrix<double> &_Matrix, const TArrayD &Chances) {
    double Max = DoubleMin;
    int MaxIdx = -1;
    for (int i = 0; i < _Matrix.size(); ++i) {
        double Res = 0;
        for (int j = 0; j < _Matrix[i].size(); ++j) {
            Res += _Matrix[i][j] * Chances[j];
        }
        std::cout << Res << " ";
        if (Res > Max) {
            Max = Res;
            MaxIdx = i;
        }
    }
    std::cout << std::endl << "Bayyesa_Laplassa";
    std::cout << std::endl << "Max = " << Max << "; Strategy -- V" << MaxIdx + 1 << std::endl;

    return MaxIdx;
}

int Valda(const Matrix<double> &Matrix) {
    double Min, Max = DoubleMin;
    int IdxOfBestStrategy = -1;
    for (int i = 0; i < Matrix.size(); ++i) {
        Min = DoubleMax;
        for (int j = 0; j < Matrix[i].size(); ++j) {
            if (Matrix[i][j] < Min) {
                Min = Matrix[i][j];
            }
        }
        if (Max < Min) {
            IdxOfBestStrategy = i;
            Max = Min;
        }
    }
    std::cout << std::endl << "Valda";
    std::cout << std::endl << "Max = " << Max << "; Strategy -- V" << IdxOfBestStrategy + 1 << std::endl;
    return IdxOfBestStrategy;
}

int Sevidga(const Matrix<double> &MatrixOfRisks) {
    double Min = DoubleMax, Max = DoubleMin;
    int IdxOfBestStrategy = -1;
    for (int i = 0; i < MatrixOfRisks.size(); ++i) {
        Max = DoubleMin;
        for (int j = 0; j < MatrixOfRisks[i].size(); ++j) {
            if (MatrixOfRisks[i][j] > Max) {
                Max = MatrixOfRisks[i][j];
            }
        }
        if (Max < Min) {
            IdxOfBestStrategy = i;
            Min = Max;
        }
    }
    std::cout << std::endl << "Sevidga";
    std::cout << std::endl << "Min = " << Min << "; Strategy -- V" << IdxOfBestStrategy + 1 << std::endl;
    return IdxOfBestStrategy;
}

int Gurvitsa(const Matrix<double> &MatrixOfRisks, double Left) {
    const double Right = 1 - Left;
    double Max = DoubleMin;
    int IdxOfBestStrategy = -1;
    for (int i = 0; i < MatrixOfRisks.size(); ++i) {
        double TempMin = MatrixOfRisks[i][0], TempMax = MatrixOfRisks[i][0];
        for (int j = 1; j < MatrixOfRisks[i].size(); ++j) {
            if (TempMax < MatrixOfRisks[i][j]) {
                TempMax = MatrixOfRisks[i][j];
            }
            if (TempMin > MatrixOfRisks[i][j]) {
                TempMin = MatrixOfRisks[i][j];
            }
        }
        const double Res = TempMin * Left + TempMax * Right;
        if (Max < Res) {
            IdxOfBestStrategy = i;
            Max = Res;
        }
    }
    std::cout << std::endl << "Gurvitsa" << " " << Left << " " << Right;
    std::cout << std::endl << "Max = " << Max << "; Strategy -- V" << IdxOfBestStrategy + 1 << std::endl;

    return IdxOfBestStrategy;
}

int Gurvitsa(const Matrix<double> &MatrixOfRisks) {
    std::cout << std::endl << "Gurvitsa";
    std::map<int, int> NumToCount;
    for (int i = 0; i < MatrixOfRisks.size(); ++i) {
        NumToCount.emplace(i, 0);
    }
    float i = 0.f;
    while (i < 1) {
        NumToCount.at(Gurvitsa(MatrixOfRisks, i))++;
        i += 0.2f;
    }
    int Max = NumToCount.at(0);
    int MaxIdx = 0;
    for (auto &El : NumToCount) {
        if (Max < El.second) {
            Max = El.second;
            MaxIdx = El.first;
        }
    }
    return MaxIdx;
}

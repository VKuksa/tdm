#ifndef Criterials_h
#define Criterials_h

#include "../types.h"
#include <iostream>

int Bayyesa_Laplassa (const Matrix<double> & _Matrix, const TArrayD & Chances);

int Valda(const Matrix<double> & Matrix);

int Sevidga(const Matrix<double> & MatrixOfRisks);

int Gurvitsa(const Matrix<double> & MatrixOfRisks, double Left);

int Gurvitsa(const Matrix<double> & MatrixOfRisk);

#endif /* Criterials_h */

#include "Criterials.h"

Matrix<double> PaymentMatrix;
TArrayD MaxValuesInPaymantMatrix;

Matrix<double> MatrixOfRisks;

static void PrintArr(const TArrayD &Arr) {
    for (auto &El : Arr) {
        std::cout << El << " ";
    }
    std::cout << std::endl;
}

template<typename T>
void Print(const Matrix<T> &MatrixToPrint);

std::vector<double> GetProfit(const TArrayD &Demands, double ProducedCount, double ProfitForObject, double PriceForObject, double ExpendituresForObject);

double FindMaxInMaxInRow(const TArrayD &Row);

double FindMaxInMatrixUsingCol(const Matrix<double> &_Matrix, int ColIdx);

TArrayD GenerateRisksRow(const TArrayD &PaymentRow, double MaxValue);

void PrintMatrixImproved(const Matrix<double> &MatrixToPrint);

namespace Task1 {
    double A1 = 250; // витрати на виробництво ящика товару
    double A2 = 500; // ціна ящику товару (по якій продають)
    double C = 5; // ціна на транспонтування
    double B1 = 55; // попит
    double B2 = 65; // попит
    double B3 = 75; // попит
    double B4 = 85; // попит
    double B5 = 95; // попит
    double P1 = 0.1; // вірогідності попиту
    double P2 = 0.15; // вірогідності попиту
    double P3 = 0.2; // вірогідності попиту
    double P4 = 0.35; // вірогідності попиту
    double P5 = 0.2; // вірогідності попиту

    const TArray<double> Demands = {B1, B2, B3, B4, B5};
    const TArray<double> DemandsChances = {P1, P2, P3, P4, P5};

    void Init() {
        const auto Size = Demands.size();
        PaymentMatrix.reserve(Size);
        MaxValuesInPaymantMatrix.reserve(Size);

        for (auto &Demand : Demands) {
            PaymentMatrix.emplace_back(GetProfit(Demands, Demand, A2, A1, C));
        }

        for (int j = 0; j < PaymentMatrix[0].size(); ++j) {
            MaxValuesInPaymantMatrix.emplace_back(FindMaxInMatrixUsingCol(PaymentMatrix, j));
        }
        /// would have been sacked for this code in the noughties
        MatrixOfRisks = PaymentMatrix;
        for (int i = 0; i < Size; ++i) {
            for (int j = 0; j < MatrixOfRisks[i].size(); ++j) {
                MatrixOfRisks[i][j] = MaxValuesInPaymantMatrix[j] - MatrixOfRisks[i][j];
            }
        }
    }
}

namespace Task2 {
    double D = 460; // Довжина маршруту (KM)

    double A1 = 12; // постачання по метру кубічному
    double A2 = 16; // постачання по метру кубічному
    double A3 = 20; // постачання по метру кубічному
    double A4 = 24; // постачання по метру кубічному
    double A5 = 28; // постачання по метру кубічному

    double C = 110; // Собівартість 1 М кубічного

    double C1 = 180; // Без запізнень
    double C2 = 170; // 1 день запізнення
    double C3 = 160; // 2 дні запізнення
    double C4 = 150; // 3 дні запізнення
    double C5 = 140; // 4 дні запізнення

    //ціна за доставку
    double H1 = 0.8; // A1         Грн/км
    double H2 = 1.0; // A2, A3, A4 Грн/км
    double H3 = 1.3; // A5         Грн/км

    double B = 55; // Втрата за просрочений день

    double P1 = 0.3;
    double P2 = 0.3;
    double P3 = 0.2;
    double P4 = 0.1;
    double P5 = 0.1;

    const TArrayD Demands = {A1, A2, A3, A4, A5};
    const TArrayD Profits = {C1, C2, C3, C4, C5};
    const TArrayD ProfitChanses = {P1, P2, P3, P4, P5};
    const TArrayD Days = {0, 1, 2, 3, 4};
    const TArrayD DeliveryPrice = {H1, H2, H2, H2, H3};

    void Init() {
        const auto Size = Demands.size();
        PaymentMatrix.resize(Size);
        for (int i = 0; i < Size; ++i) {
            PaymentMatrix[i].resize(Size);
            for (int j = 0; j < Size; ++j) {
                PaymentMatrix[i][j] = Demands[i] * (Profits[j] - C) - DeliveryPrice[i] * D - B * Days[j];
            }
        }
        for (int i = 0; i < PaymentMatrix.size(); ++i) {
            for (int j = i + 1; j < PaymentMatrix.size(); ++j) {
                int ShouldBeDeleted = 0;
                for (int k = 0; k < PaymentMatrix[0].size(); ++k) {
                    ShouldBeDeleted += (PaymentMatrix[i][k] > PaymentMatrix[j][k]) ? 1 : (PaymentMatrix[i][k] < PaymentMatrix[j][k]) ? -1 : 0;
                }
                if (ShouldBeDeleted >= (int)PaymentMatrix[0].size()) {
                    PaymentMatrix.erase(PaymentMatrix.begin() + j);
                    --j;
                } else if (ShouldBeDeleted <= -(int)PaymentMatrix[0].size()) {
                    PaymentMatrix.erase(PaymentMatrix.begin() + i);
                    --i;
                    break;
                }
            }
        }
        MaxValuesInPaymantMatrix.resize(PaymentMatrix[0].size());
        for (int i = 0; i < PaymentMatrix[0].size(); ++i) {
            double Max = PaymentMatrix[0][i];
            for (int j = 1; j < PaymentMatrix.size(); ++j) {
                if (PaymentMatrix[j][i] > Max) {
                    Max = PaymentMatrix[j][i];
                }
            }
            MaxValuesInPaymantMatrix[i] = Max;
        }
        MatrixOfRisks.resize(PaymentMatrix.size());
        for (int i = 0; i < PaymentMatrix.size(); ++i) {
            MatrixOfRisks[i].resize(PaymentMatrix[0].size());
            for (int j = 0; j < PaymentMatrix[0].size(); ++j) {
                MatrixOfRisks[i][j] = MaxValuesInPaymantMatrix[j] - PaymentMatrix[i][j];
            }
        }
    }
}

#define TASK 2

int main(int argc, const char *argv[]) {
#if TASK == 1
    Task1::Init();
#elif TASK == 2
    Task2::Init();
#endif
    PrintMatrixImproved(PaymentMatrix);
    PrintArr(MaxValuesInPaymantMatrix);
    PrintMatrixImproved(MatrixOfRisks);
#if TASK == 1
    Bayyesa_Laplassa(PaymentMatrix, Task1::DemandsChances);
#elif TASK == 2
    Bayyesa_Laplassa(PaymentMatrix, Task2::ProfitChanses);
#endif
    Valda(PaymentMatrix);
    Sevidga(MatrixOfRisks);
    Gurvitsa(MatrixOfRisks);

    std::cin.get();
    return 0;
}

std::vector<double> GetProfit(const TArrayD &Demands, double ProducedCount, double ProfitForObject, double PriceForObject, double ExpendituresForObject) {
    TArrayD Profit;
    Profit.reserve(Demands.size());
    const double ProfitPerObject = ProfitForObject - PriceForObject - ExpendituresForObject;
    for (auto &Demand : Demands) {
        if (Demand > ProducedCount) {
            Profit.emplace_back(ProducedCount * ProfitPerObject);
        } else {
            Profit.emplace_back(Demand * ProfitPerObject - (ProducedCount - Demand) * (PriceForObject + ExpendituresForObject));
        }
    }
    return Profit;
}

double FindMaxInMatrixUsingCol(const Matrix<double> &_Matrix, int ColIdx) {
    double Max = _Matrix[0][ColIdx];
    for (int i = 0; i < _Matrix.size(); ++i) {
        if (Max < _Matrix[i][ColIdx]) {
            Max = _Matrix[i][ColIdx];
        }
    }
    return Max;
}

TArrayD GenerateRisksRow(const TArrayD &PaymentRow, double MaxValue) {
    TArrayD RisksRow;
    RisksRow.reserve(PaymentRow.size());
    for (auto &El : PaymentRow) {
        RisksRow.emplace_back(MaxValue - El);
    }
    return RisksRow;
}

double FindMaxInMaxInRow(const TArrayD &Row) {
    double Max = Row[0];
    for (int i = 1; i < Row.size(); ++i) {
        if (Max < Row[i]) {
            Max = Row[i];
        }
    }
    return Max;
}

void PrintMatrixImproved(const Matrix<double> &MatrixToPrint) {
    int i = 0;
    std::cout << std::endl;
#if TASK == 1
    for (auto &El : Task1::Demands) {
        std::cout << "     " << El;
    }

#elif TASK == 2

    std::cout << " ";
    for (auto &El : Task2::Profits) {
        std::cout << "    " << El;
    }
#endif
    std::cout << std::endl;

    for (auto &Row : MatrixToPrint) {
#if TASK == 1
        std::cout << Task1::Demands[i] << " : ";
#elif TASK == 2
        std::cout << Task2::Demands[i] << " : ";
#endif

        for (auto &El : Row) {
            std::cout << El << "  ";
            if (El < 10000 && El > -1000) {
                std::cout << " ";
            } else
                continue;
            if (El < 1000 && El > -100) {
                std::cout << " ";
            } else
                continue;
            if (El < 100 && El > -10) {
                std::cout << " ";
            } else
                continue;
            if (El < 10 && El > -1) {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
        ++i;
    }
    std::cout << std::endl;
}

template<typename T>
void Print(const Matrix<T> &MatrixToPrint) {
    std::cout << std::endl;
    for (auto &Row : MatrixToPrint) {
        for (auto &El : Row) {
            std::cout << El << "  ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

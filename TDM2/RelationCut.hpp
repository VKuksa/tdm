﻿#pragma once

#include "Relation.hpp"

class RelationCut : public Relation, public std::enable_shared_from_this<Relation> {
public:
    typedef std::shared_ptr<RelationCut> PRelationCut;

    RelationCut() = default;

    explicit RelationCut(Cut);

    explicit RelationCut(IntMatrix);

    explicit RelationCut(size_t);

    ~RelationCut() = default;

    RelationCut(const RelationCut &) = default;

    RelationCut(RelationCut &&) = default;

    RelationCut &operator=(const RelationCut &) = default;

    RelationCut &operator=(RelationCut &&) = default;

    PRelation getPtr() override;

    Cut getCut() const override;

    IntMatrix getMatrix() const override;

    void print(std::string name) const override;

    void empty() override;

    void full() override;

    void diagonal() override;

    void antidiagonal() override;

    PRelation intersect(PRelation) const override;

    PRelation unify(PRelation) const override;

    PRelation subtract(PRelation) const override;

    PRelation subtractSymmetrical(PRelation) const override;

    PRelation complement() const override;

    PRelation inverse() const override;

    PRelation compose(PRelation) const override;

    PRelation narrow(size_t) const override;

    bool contain(PRelation) const override;

    bool isReflexive() const override;

    bool isAntiReflexive() const override;

    bool isSymmethrical() const override;

    bool isASymmethrical() const override;

    bool isAntiSymmethrical() const override;

    bool isTransitive() override;

    bool isAcyclic() override;

    bool isConnected() override;

    bool isTolerant() const override;

    bool isEquivalent() override;

    bool isQuasiorder() override;

    bool isOrder() override;

    bool isStrictOrder() override;

    bool isLinearOrder() override;

    bool isStrictLinearOrder() override;

    bool equal(PRelation) const override;

    PRelation symmetricComponent() override;

    PRelation asymmetricComponent() override;

    PRelation transitiveLock() override;

    PRelation reach() override;

    PRelation mutualReach() override;

    PRelation factorization(FactorizationType);

private:
    Cut cut_;
};

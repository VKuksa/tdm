﻿#include "..\types.h"
#include "Relation.hpp"
#include "RelationCut.hpp"
#include "RelationMatrix.hpp"

#include <chrono>

namespace {
    /*const IntMatrix P = {{0, 1, 1, 0, 1}, {0, 0, 1, 0, 0}, {0, 1, 0, 0, 1}, {0, 0, 1, 0, 1}, {0, 0, 0, 0, 0}};
    const IntMatrix Q = {{1, 1, 0, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 1, 1, 0}, {0, 0, 1, 1, 0}, {1, 1, 0, 0, 1}};
    const IntMatrix R = {{0, 0, 0, 1, 0}, {0, 0, 1, 1, 1}, {0, 1, 0, 0, 1}, {0, 0, 0, 1, 0}, {0, 0, 1, 1, 0}};
	*/
	const IntMatrix P = {{0, 0, 0, 0, 0}, {0, 0, 1, 0, 1}, {1, 0, 1, 0, 0}, {1, 0, 1, 0, 1}, {0, 1, 0, 0, 1}};
    const IntMatrix Q = {{0, 0, 0, 0, 0}, {0, 0, 1, 1, 0}, {0, 1, 0, 0, 1}, {0, 0, 0, 0, 0}, {0, 0, 1, 0, 0}};
    const IntMatrix R = {{1, 1, 0, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 1, 1, 0}, {0, 0, 1, 1, 0}, {1, 1, 0, 0, 1}};

    auto printProperties = [](std::shared_ptr<Relation> p) {
        std::cout << "---------------------------------------\nProperties:" << std::endl;
        std::cout << std::boolalpha;
        std::cout << "Reflexive: " << p->isReflexive() << std::endl;
        std::cout << "isAntiReflexive: " << p->isAntiReflexive() << std::endl;
        std::cout << "isSymmethrical: " << p->isSymmethrical() << std::endl;
        std::cout << "isASymmethrical: " << p->isASymmethrical() << std::endl;
        std::cout << "isAntiSymmethrical: " << p->isAntiSymmethrical() << std::endl;
        std::cout << "isTransitive: " << p->isTransitive() << std::endl;
        std::cout << "isAcyclic: " << p->isAcyclic() << std::endl;
        std::cout << "isConnected: " << p->isConnected() << std::endl;
        std::cout << "---------------------------------------" << std::endl;
    };

    auto printType = [](std::shared_ptr<Relation> p) {
        std::cout << "---------------------------------------\nType:" << std::endl;
        std::cout << std::boolalpha;
        std::cout << "isTolerant: " << p->isTolerant() << std::endl;
        std::cout << "isEquivalent: " << p->isEquivalent() << std::endl;
        std::cout << "isQuasiorder: " << p->isQuasiorder() << std::endl;
        std::cout << "isOrder: " << p->isOrder() << std::endl;
        std::cout << "isStrictOrder: " << p->isStrictOrder() << std::endl;
        std::cout << "isLinearOrder: " << p->isLinearOrder() << std::endl;
        std::cout << "isStrictLinearOrder: " << p->isStrictLinearOrder() << std::endl;
        std::cout << "---------------------------------------" << std::endl;
    };

}

int main() {
    try {
            std::shared_ptr<Relation> p = std::make_shared<RelationMatrix>(P);
            std::shared_ptr<Relation> q = std::make_shared<RelationMatrix>(Q);
            std::shared_ptr<Relation> r = std::make_shared<RelationMatrix>(R);

			q->print("Q");
            printProperties(q);
            printType(q);

			if (!q->isTransitive()) {
					q = q->transitiveLock();
					q->print("Q locked");
					printType(q);
				}

            auto pFactorized = p->factorization(Relation::FactorizationType::MutualReach);
            if (pFactorized) {
                pFactorized->print("P after factorization");
                printType(pFactorized);
             }
        
        std::cin.get();
    } catch (const std::runtime_error &exc) {
        std::cerr << "Error occured: " << exc.what() << std::endl;
    }

    return 0;
}

#pragma once

#include "Relation.hpp"

class RelationMatrix : public Relation, public std::enable_shared_from_this<RelationMatrix> {
public:
    typedef std::shared_ptr<RelationMatrix> PRelationMatrix;

    RelationMatrix() = default;

    explicit RelationMatrix(IntMatrix);

    explicit RelationMatrix(Cut);

    explicit RelationMatrix(size_t, int);

    ~RelationMatrix() = default;

    RelationMatrix(const RelationMatrix &) = default;

    RelationMatrix(RelationMatrix &&) = default;

    RelationMatrix &operator=(const RelationMatrix &) = default;

    RelationMatrix &operator=(RelationMatrix &&) = default;

    PRelation getPtr() override;

    Cut getCut() const override;

    IntMatrix getMatrix() const override;

    void print(std::string name) const override;

    void empty() override;

    void full() override;

    void diagonal() override;

    void antidiagonal() override;

    PRelation intersect(PRelation) const override;

    PRelation unify(PRelation) const override;

    PRelation subtract(PRelation) const override;

    PRelation subtractSymmetrical(PRelation) const override;

    PRelation complement() const override;

    PRelation inverse() const override;

    PRelation compose(PRelation) const override;

    PRelation narrow(size_t) const override;

    bool contain(PRelation) const override;

    bool isReflexive() const override;

    bool isAntiReflexive() const override;

    bool isSymmethrical() const override;

    bool isASymmethrical() const override;

    bool isAntiSymmethrical() const override;

    bool isTransitive() override;

    bool isAcyclic() override;

    bool isConnected() override;

    bool isTolerant() const override;

    bool isEquivalent() override;

    bool isQuasiorder() override;

    bool isOrder() override;

    bool isStrictOrder() override;

    bool isLinearOrder() override;

    bool isStrictLinearOrder() override;

    bool equal(PRelation) const override;

    PRelation symmetricComponent() override;

    PRelation asymmetricComponent() override;

    PRelation transitiveLock() override;

    PRelation reach() override;

    PRelation mutualReach() override;

    PRelation factorization(FactorizationType);

private:
    PRelationMatrix power(PRelationMatrix, size_t);

    PRelationMatrix factorize(PRelationMatrix);

    IntMatrix matrix_;
};

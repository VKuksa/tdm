﻿#include "..\types.h"
#include "Proximity.hpp"

namespace {
   /* const IntMatrix Q = {{1, 0, 0, 0, 0}, {1, 1, 1, 1, 1}, {0, 0, 1, 0, 1}, {0, 0, 0, 1, 1}, {0, 0, 0, 0, 1}};
    const Cut R = {{1, {1, 4}}, {2, {2, 5}}, {3, {3}}, {4, {4}}, {5, {5}}};

    const FloatMatrix S = {{0, 0, -1, -1, 2}, {0, 0, -1, -1, 2}, {1, 1, 0, 0, 3}, {1, 1, 0, 0, 3}, {-2, -2, -3, -3, 0}};
    const FloatMatrix T = {{0, -1, -3, -2, -1}, {1, 0, -2, -1, 0}, {3, 2, 0, 1, 2}, {2, 1, -1, 0, 1}, {1, 0, -2, -1, 0}};

    const IntMatrix Q1 = {{1, 1, 0, 0, 0}, {1, 1, 0, 0, 0}, {0, 0, 1, 0, 0}, {0, 0, 0, 1, 0}, {0, 0, 0, 0, 1}};
    const IntMatrix Q2 = {{1, 0, 0, 0, 1}, {0, 1, 0, 1, 0}, {0, 0, 1, 0, 0}, {0, 1, 0, 1, 0}, {1, 0, 0, 0, 1}};*/

	const IntMatrix Q = {{1, 0, 0, 0, 0}, {1, 1, 0, 0, 0}, {1, 1, 1, 1, 1}, {1, 1, 0, 1, 0}, {0, 0, 0, 0, 1}};
	const IntMatrix R = {{1, 0, 0, 0, 0}, {0, 1, 0, 0, 0}, {0, 1, 1, 0, 0}, {0, 0, 0, 1, 0}, {0, 0, 1, 1, 1}};

	//const Cut R = { {1, {1, 4}}, {2, {2, 5}}, {3, {3}}, {4, {4}}, {5, {5}} };

	const FloatMatrix S = { {0, -3, -3, -1, -1}, {3, 0, 0, 2, 2}, {3, 0, 0, 2, 2}, {1, -2, -2, 0, 0}, {1, -2, -2, 0, 0} };
	const FloatMatrix T = { {0, 0, 0, 1, -2}, {0, 0, 0, 1, -2}, {0, 0, 0, 1, -2}, {-1, -1, -1, 0, -3}, {2, 2, 2, 3, 0} };

	const IntMatrix Q1 = { {1, 1, 0, 0, 1}, {1, 1, 0, 0, 1}, {0, 0, 1, 0, 0}, {0, 0, 0, 1, 0}, {1, 1, 0, 0, 1} };
	const IntMatrix Q2 = { {1, 1, 0, 0, 0}, {1, 1, 0, 0, 0}, {0, 0, 1, 1, 1}, {0, 0, 1, 1, 1}, {0, 0, 1, 1, 1} };

    RelationMatrix<> q(Q, "Q");
    RelationMatrix<> r(R, "R");

    RelationMatrixMetr s(S, "S");
    RelationMatrixMetr t(T, "T");

    RelationMatrix<> q1(Q1, "Q1");
    RelationMatrix<> q2(Q2, "Q2");
}

int main() {
    try {
        //Proximity::measureProximity(q, r);
        //Proximity::measureProximity(s, t);
        Proximity::measureStructuralProximity(q1, q2);

        std::cin.get();
    } catch (const std::exception &exc) {
        std::cerr << "Error occured: " << exc.what() << std::endl;
    }

    return 0;
}

#pragma once

#include "RelationMatrixMetr.hpp"

class Proximity {
public:
    static void measureProximity(const RelationMatrix<> &first, const RelationMatrix<> &second);

    static void measureProximity(const RelationMatrixMetr &first, const RelationMatrixMetr &second);

    static void measureStructuralProximity(const RelationMatrix<> &first, const RelationMatrix<> &second);
};

﻿#include "Proximity.hpp"

namespace {
    template<typename T>
    auto countProximity = [](const T &first, const T &second) -> double {
        double result = 0;

        for (size_t i = 0; i < first.size(); ++i)
            for (size_t j = 0; j < first.size(); ++j)
                result += abs(first[i][j] - second[i][j]);

        return result / 2;
    };

    template<typename T>
    std::ostream &operator<<(std::ostream &os, const TMatrix<T> &matrix) {
        for (auto &el : matrix) {
            std::copy(el.begin(), el.end(), std::ostream_iterator<T>(os, "  "));
            std::cout << std::endl;
        }

        return os;
    }

    void printClasses(const Classes &classes) {
        for (const auto &set : classes) {
            std::cout << "{ ";
            std::copy(set.begin(), set.end(), std::ostream_iterator<int>(std::cout, " ~ "));
            std::cout << "\b\b\b }";
        }

        std::cout << "\nNumber of classes: " << classes.size() << std::endl;
    }

    decltype(auto) findClasses(const TMatrix<int> &matrix) {
        Classes classes;
        std::set<int> added;

        for (int i = 0; i < matrix.size(); ++i) {
            if (added.find(i) != added.end())
                continue;

            std::set<int> current{i + 1};
            for (int j = i + 1; j < matrix.size(); ++j) {
                if (std::equal(matrix[i].begin(), matrix[i].end(), matrix[j].begin())) {
                    current.emplace(j + 1);
                    added.emplace(j);
                }
            }
            classes.emplace_back(current);
            added.emplace(i);
        }

        return classes;
    }

    double countStructuralProximity(const TMatrix<int> &first, const TMatrix<int> &second, const TMatrix<int> &intersected) {
	    const auto firstc_ = findClasses(first);
	    const auto secondc_ = findClasses(second);
        const auto intersectedc_ = findClasses(intersected);

#ifdef OUTPUT_ALL
        printClasses(firstc_);
        printClasses(secondc_);
        printClasses(intersectedc_);
#endif

        auto getElementMaxQuantity = [&](const Classes &inputc_) {
            ElementQuantity inputsc_(inputc_.size());

            for (int i = 0; i < inputc_.size(); ++i) {
                for (const auto &elem_ : inputc_[i]) {
                    [&] {
                        for (int j = 0; j < intersectedc_.size(); ++j) {
                            for (const auto &ielem_ : intersectedc_[j]) {
                                if (elem_ == ielem_) {
                                    if (inputsc_[i] < intersectedc_[j].size()) {
                                        inputsc_[i] = intersectedc_[j].size();
                                        return;
                                    }
                                }
                            }
                        }
                    }();
                }
            }

#ifdef OUTPUT_ALL
            std::cout << "Number of elements in possible intersection:" << std::endl;
            for (auto &&elem : inputsc_) {
                std::cout << "[" << elem << "]\n";
            }
            std::cout << "----------------------" << std::endl;
#endif

            return inputsc_;
        };

        auto getSubclassesQuantity = [&](const Classes &inputc_) {
            SubclassesQuantity inputscq_(inputc_.size(), 0);

            for (int i = 0; i < inputc_.size(); ++i) {
                for (const auto &elem_ : inputc_[i]) {
                    [&] {
                        for (int j = 0; j < intersectedc_.size(); ++j) {
                            if (elem_ == *intersectedc_[j].begin()) {
                                ++inputscq_[i];
                                return;
                            }
                        }
                    }();
                }
            }

#ifdef OUTPUT_ALL
            std::cout << "Number of subclasses quantity:" << std::endl;
            for (auto &&elem : inputscq_) {
                std::cout << "[" << elem << "]\n";
            }
            std::cout << "----------------------" << std::endl;
#endif

            return inputscq_;
        };

        auto getProximity = [](const Classes &classes, const SubclassesQuantity &subclassQ, const ElementQuantity &elementQ) {
            double result = 0;

            for (int i = 0; i < subclassQ.size(); ++i) {

#ifdef OUTPUT_ALL
                std::cout << "I: " << i << "\t|P|= " << classes[i].size() << "\t|(PnR)max|= " << elementQ[i] << "\tr= " << subclassQ[i] << std::endl;
#endif
                result += 2 * (classes[i].size() - elementQ[i]) - subclassQ[i] + 1;
            }

            return result;
        };

        auto firsteq_ = getElementMaxQuantity(firstc_);
        auto secondeq_ = getElementMaxQuantity(secondc_);

        auto firstscq_ = getSubclassesQuantity(firstc_);
        auto secondscq_ = getSubclassesQuantity(secondc_);

        return getProximity(firstc_, firstscq_, firsteq_) + getProximity(secondc_, secondscq_, secondeq_);
    }
}

void Proximity::measureProximity(const RelationMatrix<> &first, const RelationMatrix<> &second) {
#ifdef OUTPUT_ALL
    std::cout << "Input \n";
    first.print();
    second.print();
#endif // OUTPUT_ALL

    auto firstm_ = first.getMetritizedMatrix();
    auto secondm_ = second.getMetritizedMatrix();

#ifdef OUTPUT_ALL
    std::cout << firstm_ << std::endl;
    std::cout << secondm_ << std::endl;
#endif // OUTPUT_ALL

    std::cout << "Result: " << countProximity<decltype(firstm_)>(firstm_, secondm_) << std::endl;
}

void Proximity::measureProximity(const RelationMatrixMetr &first, const RelationMatrixMetr &second) {
    auto firstm_ = first.getMatrix();
    auto secondm_ = second.getMatrix();

#ifdef OUTPUT_ALL
    std::cout << "Input \n";
    first.print();
    second.print();
#endif // OUTPUT_ALL

    std::cout << "Result: " << countProximity<decltype(firstm_)>(firstm_, secondm_) << std::endl;
}

void Proximity::measureStructuralProximity(const RelationMatrix<> &first, const RelationMatrix<> &second) {
    auto firstm_ = first.getMatrix();
    auto secondm_ = second.getMatrix();
    auto intersectedm_ = first.intersect(second);

#ifdef OUTPUT_ALL
    std::cout << "Input \n";
    first.print();
    second.print();
    std::cout << "Intersected: \n" << intersectedm_ << std::endl;
#endif // OUTPUT_ALL

    auto result = countStructuralProximity(firstm_, secondm_, intersectedm_);

    std::cout << "Result: " << result << std::endl;
}

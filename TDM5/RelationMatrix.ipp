#pragma once

template<typename T>
RelationMatrix<T>::RelationMatrix(Matrix m, std::string name)
    : Relation(std::move(name)) {
    size_ = m.size();
    matrix_ = std::move(m);

    for (auto &col : matrix_)
        if (col.size() != size_)
            throw std::runtime_error("Invalid matrix size");
}

template<typename T>
RelationMatrix<T>::RelationMatrix(Cut cut, std::string name)
    : Relation(std::move(name)) {
    size_ = cut.size();
    Matrix matrix(size_, std::vector<T>(size_, 0));

    for (const auto &row : cut) {
        if (!row.second.empty()) {
            for (const auto &elem : row.second)
                matrix[elem - 1][row.first - 1] = 1;
        }
    }

    matrix_ = std::move(matrix);
}

template<typename T>
RelationMatrix<T>::RelationMatrix(size_t size, Type type)
    : matrix_(size, std::vector<T>(size)) {
    size_ = size;

    switch (type) {
    case Types::Empty: {
        RelationMatrix<T>::empty();
        break;
    }
    case Types::Full: {
        RelationMatrix<T>::full();
        break;
    }
    case Types::Diagonal: {
        RelationMatrix<T>::diagonal();
        break;
    }
    case Types::Antidiagonal: {
        RelationMatrix<T>::antidiagonal();
        break;
    }
    default: {
        RelationMatrix<T>::empty();
        break;
    }
    }
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::getPtr() {
    return shared_from_this();
}

template<typename T>
typename RelationMatrix<T>::Matrix RelationMatrix<T>::getMatrix() const {
    return matrix_;
}

template<typename T>
typename RelationMatrix<T>::Matrix *RelationMatrix<T>::getMutableMatrix() {
    return matrix_;
}

template<typename T>
void RelationMatrix<T>::print(std::string info) const {
    if (matrix_.empty())
        return;

    std::cout << "Relation: " << name_.data() << info.data() << std::endl;
    for (auto &el : matrix_) {
        std::copy(el.begin(), el.end(), std::ostream_iterator<T>(std::cout, "  "));
        std::cout << std::endl;
    }
}

template<typename T>
void RelationMatrix<T>::empty() {
    for (auto &row : matrix_)
        std::fill(row.begin(), row.end(), static_cast<T>(0));
}

template<typename T>
void RelationMatrix<T>::full() {
    for (auto &row : matrix_)
        std::fill(row.begin(), row.end(), static_cast<T>(1));
}

template<typename T>
void RelationMatrix<T>::diagonal() {
    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (i == j)
                matrix_[i][j] = 1;
        }
    }
}

template<typename T>
void RelationMatrix<T>::antidiagonal() {
    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (i == j)
                matrix_[i][j] = 0;
        }
    }
}

template<typename T>
typename RelationMatrix<T>::Matrix RelationMatrix<T>::getMetritizedMatrix() const {
	Matrix result(size_, std::vector<T>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = matrix_[i][j] - matrix_[j][i];
        }
    }

	return result;
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::intersect(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<T>(size_, 0));
    auto lmatrix_ = std::dynamic_pointer_cast<RelationMatrix>(left)->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] == lmatrix_[i][j])
                result[i][j] = lmatrix_[i][j];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
typename RelationMatrix<T>::Matrix RelationMatrix<T>::intersect(RelationMatrix<T> left) const {
    if (left.size() != matrix_.size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return RelationMatrix<T>::Matrix();
    }

    Matrix result(size_, std::vector<T>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] == left.matrix_[i][j])
                result[i][j] = left.matrix_[i][j];
        }
    }

    return result;
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::unify(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<T>(size_, 0));
    auto lmatrix_ = std::dynamic_pointer_cast<RelationMatrix>(left)->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if ((this->matrix_[i][j] == 1) || (lmatrix_[i][j] == 1))
                result[i][j] = 1;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::subtract(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<T>(size_, 0));
    auto lmatrix_ = std::dynamic_pointer_cast<RelationMatrix>(left)->getMatrix();

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (this->matrix_[i][j] != lmatrix_[i][j])
                result[i][j] = this->matrix_[i][j];
            else
                result[i][j] = 0;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::subtractSymmetrical(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    auto first = this->subtract(left);
    auto second = left->subtract(std::const_pointer_cast<RelationMatrix>(shared_from_this()));

    return first->unify(second);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::complement() const {
    Matrix result(size_, std::vector<T>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = 1 - matrix_[i][j];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::inverse() const {
    Matrix result(size_, std::vector<T>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = matrix_[j][i];
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::compose(PRelation left) const {
    if (left->size() != this->size()) {
        std::cerr << "Invalid matrix size provided" << std::endl;
        return PRelation();
    }

    auto lmatrix_ = std::dynamic_pointer_cast<RelationMatrix>(left)->getMatrix();
    Matrix result(size_, std::vector<T>(size_, 0));

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            result[i][j] = 0;
            for (size_t k = 0; k < size_; ++k) {
                result[i][j] += this->matrix_[i][k] * lmatrix_[k][j];
            }
            if (result[i][j] > 1)
                result[i][j] = 1;
        }
    }

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::narrow(size_t excluded) const {
    if (excluded > size_) {
        std::cerr << "Invalid exclude parameter provided" << std::endl;
        return PRelation();
    }
    if (size_ == 1) {
        std::cerr << "Matrix cannot be narrowed" << std::endl;
        return PRelation();
    }

    Matrix result(size_, std::vector<T>(size_ - 1, 0));

    for (size_t i = 0; i < size_; ++i) {
        if (i == excluded - 1)
            continue;
        for (size_t j = 0; j < size_ - 1; ++j) {
            result[i][j] = matrix_[i][j >= excluded - 1 ? j + 1 : j];
        }
    }
    result.erase(result.begin() + excluded - 1);

    return std::make_shared<RelationMatrix>(result);
}

template<typename T>
bool RelationMatrix<T>::contain(PRelation left) const {
    Matrix lmatrix_(std::dynamic_pointer_cast<RelationMatrix>(left)->getMatrix());

    for (size_t i = 0; i < size_; ++i) {
        for (size_t j = 0; j < size_; ++j) {
            if (lmatrix_[i][j] == 1 && lmatrix_[i][j] != matrix_[i][j])
                return false;
        }
    }

    return true;
}

template<typename T>
bool RelationMatrix<T>::isReflexive() const {
    for (size_t i = 0; i < size(); ++i) {
        if (matrix_[i][i] != 1)
            return false;
    }
    return true;
}

template<typename T>
bool RelationMatrix<T>::isAntiReflexive() const {
    for (size_t i = 0; i < size(); ++i) {
        if (matrix_[i][i] != 0)
            return false;
    }
    return true;
}

template<typename T>
bool RelationMatrix<T>::isSymmethrical() const {
    for (size_t i = 0; i < size(); ++i) {
        for (size_t j = i + 1; j < size(); ++j) {
            if (matrix_[i][j] != matrix_[j][i])
                return false;
        }
    }
    return true;
}

template<typename T>
bool RelationMatrix<T>::isASymmethrical() const {
    auto empty = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<T>(size_, 0)));
    empty->empty();

    auto self = shared_from_this();
    auto inverse = self->inverse();
    return self->intersect(inverse)->equal(empty);
}

template<typename T>
bool RelationMatrix<T>::isAntiSymmethrical() const {
    for (size_t i = 0; i != size(); i++) {
        for (size_t j = 0; j != size(); j++) {
            if ((i != j) && matrix_[i][j] != 0 && matrix_[j][i] != 0) {
                return false;
            }
        }
    }
    return !isASymmethrical();
}

template<typename T>
bool RelationMatrix<T>::isTransitive() {
    auto compose = shared_from_this()->compose(shared_from_this());
    return compose->contain(shared_from_this());
}

template<typename T>
bool RelationMatrix<T>::isAcyclic() {
    auto empty = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<T>(size_, 0)));
    empty->empty();
    return shared_from_this()->compose(shared_from_this())->intersect(shared_from_this()->inverse())->equal(empty);
}

template<typename T>
bool RelationMatrix<T>::isConnected() {
    auto self = shared_from_this();
    auto inversedSelf = self->inverse();

    auto diagonal = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<T>(size_, 0)));
    diagonal->diagonal();

    auto full = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<T>(size_, 0)));
    full->full();

    return (self->unify(inversedSelf)->subtract(diagonal)->equal(full->subtract(diagonal)));
}

template<typename T>
bool RelationMatrix<T>::isTolerant() const {
    return (this->isReflexive() && this->isSymmethrical());
}

template<typename T>
bool RelationMatrix<T>::isEquivalent() {
    return (this->isTolerant() && this->isTransitive());
}

template<typename T>
bool RelationMatrix<T>::isQuasiorder() {
    return (this->isReflexive() && this->isTransitive());
}

template<typename T>
bool RelationMatrix<T>::isOrder() {
    return (this->isQuasiorder() && this->isAntiSymmethrical());
}

template<typename T>
bool RelationMatrix<T>::isStrictOrder() {
    return (this->isASymmethrical() && this->isTransitive());
}

template<typename T>
bool RelationMatrix<T>::isLinearOrder() {
    return (this->isOrder() && this->isConnected());
}

template<typename T>
bool RelationMatrix<T>::isStrictLinearOrder() {
    return (this->isStrictOrder() && this->isConnected());
}

template<typename T>
bool RelationMatrix<T>::equal(PRelation left) const {
    if (left->size() != this->size())
        return false;

    auto lmatrix_ = std::dynamic_pointer_cast<RelationMatrix>(left)->getMatrix();

    for (size_t i = 0; i < size(); ++i) {
        for (size_t j = 0; j < size(); ++j) {
            if (matrix_[i][j] != lmatrix_[i][j])
                return false;
        }
    }
    return true;
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::symmetricComponent() {
    auto self = shared_from_this();
    auto inversedSelf = self->inverse();

    return self->intersect(inversedSelf);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::asymmetricComponent() {
    auto self = shared_from_this();
    auto selfSymmetciComponent = self->symmetricComponent();

    return self->subtract(selfSymmetciComponent);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::transitiveLock() {
    auto self = shared_from_this();

    for (size_t i = 2; i < this->size(); ++i) {
        self = std::dynamic_pointer_cast<RelationMatrix>(self->unify(power(self, i)));
    }

    return self;
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::reach() {
    auto diagonal = std::make_shared<RelationMatrix>(Matrix(size_, std::vector<T>(size_, 0)));
    diagonal->diagonal();

    return this->transitiveLock()->unify(diagonal);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::mutualReach() {
    auto reach = this->reach();
    auto inversed = reach->inverse();
    return reach->intersect(inversed);
}

template<typename T>
typename RelationMatrix<T>::PRelation RelationMatrix<T>::factorization(FactorizationType type) {
    std::shared_ptr<Relation> d;
    switch (type) {
    case FactorizationTypes::MutualReach:
        d = this->mutualReach();
        std::cout << "Using mutual reach: \n";
        break;
    case FactorizationTypes::SymmetricComponent:
        d = this->symmetricComponent();
        std::cout << "Using symmetric component: \n";
        break;
    default:
        d = shared_from_this();
        break;
    }

    return factorize(std::dynamic_pointer_cast<RelationMatrix>(d));
}

template<typename T>
typename RelationMatrix<T>::PRelationMatrix RelationMatrix<T>::power(PRelationMatrix selfBasic, size_t current) {
    if (current == 1) {
        return selfBasic;
    } else {
        return std::dynamic_pointer_cast<RelationMatrix>(selfBasic->compose(power(selfBasic, current - 1)));
    }
}

template<typename T>
typename RelationMatrix<T>::PRelationMatrix RelationMatrix<T>::factorize(PRelationMatrix d) {
    if (!d)
        return PRelationMatrix();

    auto p = shared_from_this();
    auto dmatrix_ = d->getMatrix();

    std::list<std::set<size_t>> factors;
    std::set<size_t> added;
    p->print("P");
    d->print("D");

    for (size_t i = 0; i < d->size() - 1; ++i) {
        if (std::find(added.begin(), added.end(), i) != added.end())
            continue;

        std::set<size_t> current{i};
        for (size_t j = i + 1; j < d->size(); ++j) {
            if (std::equal(dmatrix_[i].begin(), dmatrix_[i].end(), dmatrix_[j].begin())) {
                current.emplace(j);
                added.emplace(j);
            }
        }
        factors.emplace_back(current);
        added.emplace(i);
    }

    auto result = std::dynamic_pointer_cast<RelationMatrix>(p->inverse())->getMatrix();
    for (const auto &set : factors) {
        if (set.size() > 1) {
            for (auto it = ++set.begin(); it != set.end(); ++it) {
                auto firstCol = result.at(*--it);
                auto secondtCol = result.at(*++it);
                std::vector<T> resultCol(size(), 0);
                for (int i = 0; i < size(); ++i) {
                    if ((firstCol[i] == 1) || (secondtCol[i] == 1))
                        resultCol[i] = 1;
                }
                result[*--it] = std::move(resultCol);
                ++it;
            }
        }
    }

    auto cresult = Matrix(factors.size());
    auto rit = result.begin();
    for (auto it = cresult.begin(); it != cresult.end(); ++it, ++rit) {
        *it = std::vector<T>(rit->begin(), rit->begin() + factors.size());
    }

    return std::dynamic_pointer_cast<RelationMatrix>(std::make_shared<RelationMatrix>(cresult)->inverse());
}

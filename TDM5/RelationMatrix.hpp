#pragma once

#include "Relation.hpp"

#include <iterator>

template<typename T = int>
class RelationMatrix : public Relation, public std::enable_shared_from_this<RelationMatrix<T>> {
public:
    typedef std::shared_ptr<RelationMatrix> PRelationMatrix;

    typedef TMatrix<T> Matrix;

    RelationMatrix() = default;

    explicit RelationMatrix(Matrix, std::string = "");

    explicit RelationMatrix(Cut cut, std::string = "");

    explicit RelationMatrix(size_t size, Type type);

    ~RelationMatrix() = default;

    RelationMatrix(const RelationMatrix &) = default;

    RelationMatrix(RelationMatrix &&) = default;

    RelationMatrix &operator=(const RelationMatrix &) = default;

    RelationMatrix &operator=(RelationMatrix &&) = default;

    PRelation getPtr() override;

    Matrix getMatrix() const;

    Matrix *getMutableMatrix();

    void print(std::string = "") const override;

    void empty() override;

    void full() override;

    void diagonal() override;

    void antidiagonal() override;

    PRelation intersect(PRelation) const override;

    Matrix intersect(RelationMatrix<T>) const;

    PRelation unify(PRelation) const override;

    PRelation subtract(PRelation) const override;

    PRelation subtractSymmetrical(PRelation) const override;

    PRelation complement() const override;

    PRelation inverse() const override;

    PRelation compose(PRelation) const override;

    PRelation narrow(size_t) const override;

    Matrix getMetritizedMatrix() const;

    bool contain(PRelation) const override;

    bool isReflexive() const override;

    bool isAntiReflexive() const override;

    bool isSymmethrical() const override;

    bool isASymmethrical() const override;

    bool isAntiSymmethrical() const override;

    bool isTransitive() override;

    bool isAcyclic() override;

    bool isConnected() override;

    bool isTolerant() const override;

    bool isEquivalent() override;

    bool isQuasiorder() override;

    bool isOrder() override;

    bool isStrictOrder() override;

    bool isLinearOrder() override;

    bool isStrictLinearOrder() override;

    bool equal(PRelation) const override;

    PRelation symmetricComponent() override;

    PRelation asymmetricComponent() override;

    PRelation transitiveLock() override;

    PRelation reach() override;

    PRelation mutualReach() override;

    PRelation factorization(FactorizationType) override;

protected:
    PRelationMatrix power(PRelationMatrix, size_t);

    PRelationMatrix factorize(PRelationMatrix);

    Matrix matrix_;
};

#include "RelationMatrix.ipp"
